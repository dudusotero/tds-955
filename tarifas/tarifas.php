<?php

/**
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Classe tarifas - Implementa as ações da tarifa como cadastro,edição e exclusão
 *
 * @category  Snep
 * @package   tarifas_tarifas
 * @copyright Copyright (c) 2010 OpenS Tecnologia
 * @author Opens Tecnologia
 */
require_once("../includes/verifica.php");
require_once("../configs/config.php");
ver_permissao(45);

$row_oper = Snep_Operadoras::getAll();

unset($val);
$operadoras = array('' => $LANG['undef']);

foreach ($row_oper as $val) {
    $operadoras[$val['codigo']] = $val['nome'];
}
asort($operadoras);

$acao = (isset($acao) ? $acao : $acao = "");

$smarty->assign('OPERADORAS', $operadoras);
$smarty->assign('ESTADOS', $uf_brasil);
$smarty->assign('PROTOTYPE', true);
$smarty->assign('ACAO', $acao);

if ($acao == "cadastrar") {
    cadastrar();
} elseif ($acao == "alterar") {
    $titulo = $LANG['menu_tarifas']. " » " . $LANG['menu_tarifas_reajuste'] . " » " . $LANG['change'];
    alterar();
} elseif ($acao == "grava_alterar") {
    grava_alterar();
} elseif ($acao == "excluir") {
    excluir();
} else {
    $titulo = $LANG['menu_tarifas']. " » " . $LANG['menu_tarifas_reajuste'] . " » " . $LANG['include'];
    principal();
}

/**
 * principal - Monta a tela principal da rotina
 * @global type $smarty
 * @global type $titulo
 * @global type $LANG
 */
function principal() {
    global $smarty, $titulo, $LANG;

    $smarty->assign('TARIFAS', 'TRUE');
    $smarty->assign('ACAO', "cadastrar");
    $smarty->assign('CITY', $LANG['select']);
    display_template("tarifas.tpl", $smarty, $titulo);
}

/**
 * cadastrar - Inclui nova tarifa no sistema
 * @global type $LANG
 */
function cadastrar() {
    global $LANG;

    $tarifa = new Snep_Tarifas();
    $tarifa->operadora = $_POST['operadora'];
    $tarifa->ddi = $_POST['ddi'];
    $tarifa->pais = $_POST['pais'];
    $tarifa->ddd = $_POST['ddd'];
    $tarifa->cidade = $_POST['cidade'];
    $tarifa->estado = $_POST['estado'];
    $tarifa->prefixo = $_POST['prefixo'];
    $tarifa->vcel = $_POST['vcel'];
    $tarifa->vfix = $_POST['vfix'];
    $tarifa->vpf = 0; //$_POST['vpf'];
    $tarifa->vpc = 0; //$_POST['vpc'];

    Snep_Tarifas::register($tarifa);
    
    //log
    $tabela = "";
    $tabela = verificaLog($tabela);
    if ($tabela == true) {

        $cidade = $_POST['cidade'];
        $db = Zend_Registry::get('db');
        $sql = "SELECT codigo from tarifas where tarifas.cidade = '$cidade'";
        $stmt = $db->query($sql);
        $cidade = $stmt->fetchAll();
        $id = $cidade[0]["codigo"];
        $add = getTarifa($id);
        $lastId = getLastId();
        $add["codigo"] = $lastId;
        $action = "ADD";
        insertLogTarifa($action, $add);
        $acao = "Adicionou tarifa";
        salvalog($acao, $lastId);
    }


    echo "<meta http-equiv='refresh' content='0;url=../tarifas/tarifas.php'>\n";
}

exit;

/**
 * getTarifa - Monta array com todos dados da tarifa
 * @param <int> $id - Código da tarifa
 * @return <array> $tarifa - Dados da tarifa
 */
function getTarifa($id) {

    $tarifa = array();

    $db = Zend_Registry::get("db");
    $sql = "SELECT operadora, ddi, pais, ddd, cidade, estado, prefixo, data, vcel, vfix from  tarifas, tarifas_valores where tarifas.codigo = '$id' AND tarifas_valores.codigo ='$id' order by tarifas_valores.data desc";
    $stmt = $db->query($sql);
    $tarifa = $stmt->fetch();
    return $tarifa;
}

/**
 * insertLogTarifa - insere na tabela logs_tarifas as tarifas
 * @global <int> $id_user
 * @param <array> $add
 */
function insertLogTarifa($acao, $add) {

    $db = Zend_Registry::get("db");
    $ip = $_SERVER['REMOTE_ADDR'];
    $hora = date('Y-m-d H:i:s');

    $auth = Zend_Auth::getInstance();
    global $id_user;
    $tipo = 4;
    $operadora = $add["operadora"];

    $select = "SELECT name from peers where id = '$id_user'";
    $stmt = $db->query($select);
    $id = $stmt->fetch();

    $select = "SELECT nome from operadoras where codigo = '$operadora'";
    $stmt = $db->query($select);
    $operadora = $stmt->fetch();

    $sql = "INSERT INTO `logs_tarifas` VALUES (NULL, '" . $hora . "', '" . $ip . "', '" . $id["name"] . "', '" . $operadora['nome'] . "', '" . $add["ddi"] . "', '" . $add["pais"] . "', '" . $add["ddd"] . "', '" . $add["cidade"] . "', '" . $add["estado"] . "', '" . $add["prefixo"] . "', '" . $add["codigo"] . "', '" . $add["data"] . "', '" . $add["vcel"] . "', '" . $add["vfix"] . "', '" . $acao . "')";
    $db->query($sql);
}

/**
 * getLastId - Busca ID da ultima tarifa adicionada
 * @return <int> $result - Código da última tarifa
 */
function getLastId() {

    $db = Zend_Registry::get("db");
    $sql = "SELECT codigo from  tarifas order by codigo desc limit 1";
    $stmt = $db->query($sql);
    $result = $stmt->fetch();

    return $result["codigo"];
}

/**
 * alterar - Alterar registro selecionado 
 * @global type $LANG
 * @global type $smarty
 * @global type $titulo
 * @global <string> $acao
 */
function alterar() {
    global $LANG, $smarty, $titulo, $acao;

    $codigo = isset($_POST['codigo']) ? $_POST['codigo'] : $_GET['codigo'];

    if (!$codigo) {
        display_error($LANG['msg_notselect'], true);
        exit;
    }

    $row_vlr = Snep_Tarifas::getValor($codigo);
    $row = Snep_Tarifas::get($codigo);

    $smarty->assign('ACAO', "grava_alterar");
    $smarty->assign('dt_tarifas', $row[0]);
    $smarty->assign('ESTADO', $row[0]['estado']);
    $smarty->assign('CIDADE', $row[0]['cidade']);
    $smarty->assign('dt_valores', $row_vlr);
    $smarty->assign('id_tarifa', $row_vlr[0]['codigo']);
    $smarty->assign('CITY', $LANG['select']);
    display_template("tarifas.tpl", $smarty, $titulo);
}

/**
 * grava_alterar - Grava registro alterado somente para tarifas_valores 
 * @global type $LANG
 * @global type $action
 */
function grava_alterar() {
    global $LANG, $action;

    if (!$_POST['codigo']) {
        display_error($LANG['msg_notselect'], true);
        exit;
    }
    
    //log
    $tabela = "";
    $tabela = verificaLog($tabela);
    if ($tabela == true) {

        $db = Zend_Registry::get('db');
        $old = getTarifa($_POST['codigo']);
        $action_ = "OLD";
        $old['codigo'] = $_POST['codigo'];
        insertLogTarifa($action_, $old);
    }

    foreach ($action as $id => $tar) {

        $tarifa = new Snep_Tarifas();
        $tarifa->data = $_POST['data'][$action[$tar]];
        $tarifa->vcel = $_POST['vcel'][$action[$tar]];
        $tarifa->vfix = $_POST['vfix'][$action[$tar]];
        $tarifa->vpf = 0; //$_POST['vpf'][$action[$tar]];
        $tarifa->vpc = 0; //$_POST['vpc'][$action[$tar]];
        $edit = 1;
        Snep_Tarifas::registerValores($tarifa, $_POST['codigo'], $edit);

        unset($tarifa);
    }

    echo "<meta http-equiv='refresh' content='0;url=../tarifas/rel_tarifas.php'>\n";
    exit;
}

/**
 * excluir - Exclui registro selecionado 
 * @global type $LANG
 */
function excluir() {
    global $LANG;

    $codigo = isset($_POST['codigo']) ? $_POST['codigo'] : $_GET['codigo'];

    if (!$codigo) {
        display_error($LANG['msg_notselect'], true);
        exit;
    }
    
    //log
    $tabela = "";
    $tabela = verificaLog($tabela);
    if ($tabela == true) {

        $db = Zend_Registry::get('db');
        $del = getTarifa($codigo);
        $action = "DEL";
        $del["codigo"] = $codigo;
        insertLogTarifa($action, $del);
        $acao = "Removeu tarifa";
        salvalog($acao, $codigo);
    }

    Snep_Tarifas::remove($codigo);

    echo "<meta http-equiv='refresh' content='0;url=../tarifas/rel_tarifas.php'>\n";
}

/**
 * verificaLog - Verifica se existe módulo Loguser
 * @param <boolean> $tabela
 * @return <boolean> True ou false
 */
function verificaLog($tabela) {
    if (class_exists("Loguser_Manager")) {
        $tabela = true;
    } else {
        $tabela = false;
    }
    return $tabela;
}

/**
 * salvalog - Insere dados da ação na tabela logs
 * @global type $id_user
 * @param <String> $ação - Ação feita pelo usuário
 * @param <int> $id - Id da tarifa
 * @return <boolean> True ou false
 */
function salvaLog($acao, $id) {
    $db = Zend_Registry::get("db");
    $ip = $_SERVER['REMOTE_ADDR'];
    $hora = date('Y-m-d H:i:s');
    $tipo = 4;
    global $id_user;

    $acao = mysql_escape_string($acao);

    $sql = "INSERT INTO `logs` VALUES (NULL, '" . $hora . "', '" . $ip . "', '" . $id_user . "', '" . $acao . "', '" . $id . "', '" . $tipo . "', '" . NULL . "', '" . NULL . "', '" . NULL . "', '" . NULL . "')";

    if ($db->query($sql)) {
        return true;
    } else {
        return false;
    }
}
