{*
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 *}
 {include file="cabecalho.tpl"}
 <table>
    <form method="post" name="filas">
 <input type="hidden" name="vinculos" id="vinculos" value="{$VINCULOS}"/>
    <tr style="background-color: #f1f1f1;">
       <td class="esq" width="30%">
               {$LANG.access_level} :
       </td>
       <td class="esq">
                {if $NIVEL == ""}
                    {$LANG.stnone}
                {elseif $NIVEL == 1}
                    {$LANG.vinculos_todos}
                {else}
                    {$NIVEL}
                    {if $VALOR.botao != null}
                        {if $VALOR.botao == "+"}    

                            <input class="button" type="submit" id="submit" name="visualizacao" value={"+"}>
                            <div class="buttonEnding"></div>

                        {else if $VALOR.botao == "-"}

                            <input class="button" type="submit" id="submit" name="visualizacao" value={"-"}>
                            <div class="buttonEnding"></div>

                        {/if}
                    {/if}
                {/if}
            </td>
    </tr>
   <tr>
    <tr>
       <td class="esq" width="30%">
        {$LANG.periodo}
       </td>
       <td class="esq">
          <table class="subtable">
             <tr>
                <td class="subtable" width="15%">
                   {$LANG.apartir} :
                </td>
                <td class="subtable">
                    <input type="text"  class="datepicker" name="dataini" id="dataini" value="{$dt_relservices.dia_ini}">
                    <input type="hidden" id="dia_ini" name="dia_ini"> 
                </td>
             </tr>
             <tr>
                <td class="subtable">
                  {$LANG.ate} :
                </td>
                <td class="subtable">
                    <input type="text" class="datepicker" name="datafim" id="datafim" value="{$dt_relservices.dia_fim}">
                    <input type="hidden" id="dia_fim" name="dia_fim"> 
                </td>
             </tr>
          </table>
       </td>
    <tr>
       <td class="esq">
          {$LANG.envolved_callers}&nbsp;
       </td>
       <td class="esq">
            {$LANG.group} :
            <select name="groupsrc" class="campos" onChange="javascript:grupos('fls', this.value)">
                 {html_options options=$OPCOES_USERGROUPS selected=$groupsrc}
            </select>
          &nbsp;&nbsp;
            <input type="text" name="src" id="src" class="campos" value="{$dt_relservices.src}"  >
            {$LANG.morethatone}
       </td>
    </tr>

    <tr>
       <td class="esq">
          {$LANG.services_list}&nbsp;
       </td>
       <td class="esq">
            {foreach from=$SERVICES item=service name=services}
                <input name="services[]" type="checkbox" checked="yes" value="{$service}"> {$service}
            {/foreach}
       </td>
    </tr>

    <tr>
       <td class="esq">
          {$LANG.service_state}&nbsp;
       </td>
       <td class="esq">
                <input name="state[]" type="checkbox" checked="yes" value="1"> {$LANG.service_enable}
                <input name="state[]" type="checkbox" checked="yes" value="0"> {$LANG.service_disable}
       </td>
    </tr>


    <tr class="cen">
       <td colspan="3" height="40">     
          <input type="hidden" id="acao" name="acao" value="">

          <input class="button" type="submit" name="relatorio" id="relatorio" value="{$LANG.viewreport}" 
                 OnClick="ajustavalor(); document.filas.acao.value='relatorio';document.getElementById('frescura').style.display='block'">
          <div class="buttonEnding"></div>
          &nbsp;&nbsp;&nbsp;
          <input class="button" type="submit" name="csv" id="csv" value="{$LANG.viewcsv}"  
                 OnClick="ajustavalor(); document.filas.acao.value='csv';document.getElementById('frescura').style.display='block'">
          <div class="buttonEnding"></div>
          &nbsp;&nbsp;&nbsp;

          <div align="center" id="frescura" style="display : none;">
              <img src="../imagens/ajax-loader2.gif" width="256" height="24" /><br />
            {$LANG.processing}
          </div>
       </td>
    </tr>
 </form>
 </table>
 { include file="rodape.tpl }
 <script type="text/javascript">
    new Control.DatePicker('dataini', {ldelim}icon: '../imagens/calendar.png', timePicker: true, timePickerAdjacent: true, locale: 'pt-BR'{rdelim});
    new Control.DatePicker('datafim', {ldelim}icon: '../imagens/calendar.png', timePicker: true, timePickerAdjacent: true, locale: 'pt-BR'{rdelim});
    document.forms[0].elements[0].focus() ;
    function ajustavalor() {ldelim}
        document.getElementById('dia_ini').value = document.getElementById('dataini').value ;
        document.getElementById('dia_fim').value = document.getElementById('datafim').value ;
    {rdelim}
 </script>

