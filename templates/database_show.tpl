{*
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 *}
{config_load file="../includes/setup.conf" section="cores"}
<table cellpadding="0" cellspacing="0" border="0" align="center">
   <thead>
      <tr>
         <td>{$LANG.ramais|upper}</td>
         <td>{$LANG.queues|upper}</td>
      </tr>
   </thead>
   <tr>
     <!-- Ramais -->
     <td valign="top">
        <table>
           <thead>
              <tr>
                 <td class="esq">{$LANG.ramal}</td>
                 <td class="cen">{$LANG.type}</td>
                 <td class="esq">{$LANG.ip}</td>
                 <td class="esq">{$LANG.latencia}</td>
                 <!-- <td class="esq">{$LANG.codecs}</td> -->
              </tr>
           </thead>
           {foreach name=ramais from=$RAMAIS key=curr_key item=curr_item}
              <tr bgcolor='{cycle values="`$smarty.config.COR_GRID_A`,`$smarty.config.COR_GRID_B`"}'>
                 <td class="esq">{$curr_item.ramal}</td>
                 <td class="cen">{$curr_item.tipo}</td>
                 <td class="esq">{$curr_item.ip}</td>
                 <td class="esq">{$curr_item.delay}</td>
                 <!-- <td class="esq">{$curr_item.codec}</td> -->
              </tr>
           {/foreach}
        </table>
     </td>

     <td valign="top">
        <table>
           <thead>
              <tr>
                 <td class="esq">{$LANG.row}</td>
                 <td class="cen">{$LANG.queue_members}</td>
                 <td class="cen">{$LANG.calls_waiting}</td>
              </tr>
           </thead>
           {foreach name=agentes from=$FILAS key=curr_key item=curr_item}
               <tr>
                   <td class="esq" style="vertical-align: top;">{$curr_item.name}</td>
                   <td class="cen" style="vertical-align: top;">{$curr_item.members}</td>
                   <td class="cen" style="vertical-align: top;">{$curr_item.calls}</td>
              </tr>
           {/foreach}
        </table>
        <br />
        <table>
           <thead>
              <tr>
                 <td class="esq">{$LANG.licenses}</td>
                 <td class="esq">{$LANG.encode}</td>
                 <td class="esq">{$LANG.decode}</td>
              </tr>
           </thead>
           {if $CODECS}
              <tr bgcolor='{cycle values="`$smarty.config.COR_GRID_A`,`$smarty.config.COR_GRID_B`"}'>
                 <td class="esq" style="vertical-align: top;">{$CODECS.0}</td>
                 <td class="esq">{$CODECS.1}</td>
                 <td class="esq">{$CODECS.2}</td>
              </tr>
           {/if}
        </table>
        <br />
        
        <!-- Tabela de Troncos -->
        <table>
           <thead>
              <th colspan='6'>
                 Troncos SIP
              </th>
              <tr>
	        <td>Nome</td>
              	<td>{$LANG.ip}</td>
                <td>{$LANG.alert_user}</td>
                <td>{$LANG.type}</td>
              	<td>{$LANG.status}</td>
              	<td>{$LANG.latencia}</td>
              </tr>
           </thead>
           {foreach name=TRONCOS from=$TRONCOS key=trunk_key item=trunk_val}
              <tr>
              	{foreach name=PEERS from=$trunk_val key=peer_key item=peer_val }
                 	<td class="esq" style="vertical-align: top;">{$peer_val}<br/></td>
                {/foreach}
              </tr>
           {/foreach}
        </table>
        <br/>
        <!-- Tabela de Troncos IAX-->
        <table>
           <thead>
              <th colspan='6'>
                 Troncos IAX
              </th>
              <tr>
	        <td>{$LANG.name}</td>
              	<td class="cen">{$LANG.ip}</td>
                <td class="cen">{$LANG.alert_user}</td>
              	<td class="cen">{$LANG.type}</td>
              	<td class="cen">{$LANG.status}</td>
                <td class="cen">{$LANG.latencia}</td>
              </tr>
           </thead>
           {foreach name=IAX2 from=$IAX2 key=iax2_key item=iax2_val}
              <tr>
              	{foreach name=PEERS from=$iax2_val key=peer_key item=peer_val }
                 	<td class="esq" style="vertical-align: top;">{$peer_val}<br/></td>
                {/foreach}
              </tr>
           {/foreach}
        </table>
     </td>
   </tr>   
</table>
{ include file="rodape.tpl" }
