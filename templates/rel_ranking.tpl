{*
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 *}
{include file="cabecalho.tpl"}
<table>
   <form method="post" name="relatorio">
    <input type="hidden" name="vinculos" id="vinculos" value="{$VINCULOS}"/>
    <tr style="background-color: #f1f1f1;">
       <td class="esq" width="30%">
               {$LANG.access_level} :
       </td>
       <td class="esq">
           {if $NIVEL == ""}
               {$LANG.stnone}
           {elseif $NIVEL == 1}
               {$LANG.vinculos_todos}
           {else}
               {$NIVEL}
           {/if}
       </td>
    </tr>
   <tr>
      <td class="esq" width="30%">
         {$LANG.periodo}
      </td>
      <td class="esq">
         <table class="subtable">
            <tr>
               <td class="subtable" width="15%">
                  {$LANG.apartir} :
               </td>
               <td class="subtable">
                    <input type="text"  class="datepicker" name="dataini" id="dataini" value="{$dt_ranking.dia_ini}">
                    <input type="hidden" id="dia_ini" name="dia_ini"> 
               </td>
            </tr>
            <tr>
               <td class="subtable">
                  {$LANG.ate} :
               </td>
               <td class="subtable">
                    <input type="text" class="datepicker" name="datafim" id="datafim" value="{$dt_ranking.dia_fim}">
                    <input type="hidden" id="dia_fim" name="dia_fim"> 
               </td>
            </tr>
         </table>
      </td>
   </tr>
   <tr>
       <td class="esq">
          {$LANG.rank_type}
       </td>
       <td class="esq">
          {html_radios name="rank_type" checked="$rank_type" options=$OPCOES_RANK}
       </td>
   </tr>
   <tr>
       <td class="esq">
          {$LANG.rank_viewsrc}
       </td>
       <td class="esq">
          <input type="text" size="4" maxlength="5" class="campos" name="rank_num" value="{$rank_num}" />
       </td>
   </tr>
   <tr>
       <td class="esq">
          {$LANG.rank_viewtop}
       </td>
       <td class="esq">
          <select name="viewtop" class="campos">
             {html_options options=$VIEWTOP selected=$viewtop}
          </select>
       </td>
    </tr>         
   <tr>
       <td class="esq">
          {$LANG.rank_viewrec}
       </td>
       <td class="esq">
          {html_radios name="viewrec" checked="$viewrec" options=$OPCOES_REC}
       </td>
    </tr>  
   <tr class="cen">
      <td colspan="3" height="40">     
         <input type="hidden" id="acao" name="acao" value="">
         <input class="button" type="submit" name="submit" id="submit" value="{$LANG.viewreport}" 
                OnClick="ajustavalor();document.relatorio.acao.value='relatorio';document.getElementById('frescura').style.display='block'">
         <div class="buttonEnding"></div>
            &nbsp;&nbsp;&nbsp;
         <input class="button" type="submit" name="csv" id="csv" value="{$LANG.viewcsv}" 
                OnClick="ajustavalor();document.relatorio.acao.value='csv';document.getElementById('frescura').style.display='block'">
         <div class="buttonEnding"></div>

         <div align="center" id="frescura" style="display : none;">
            <img src="../imagens/ajax-loader2.gif" width="256" height="24" /><br />
            {$LANG.waitreport}            
         </div>
      </td>
   </tr>
</form>
</table>
{ include file="rodape.tpl }
<script type="text/javascript">
    new Control.DatePicker('dataini', {ldelim}icon: '../imagens/calendar.png', timePicker: true, timePickerAdjacent: true, locale: 'pt-BR'{rdelim});
    new Control.DatePicker('datafim', {ldelim}icon: '../imagens/calendar.png', timePicker: true, timePickerAdjacent: true, locale: 'pt-BR'{rdelim});
    document.forms[0].elements[0].focus() ;
    function ajustavalor() {ldelim}
        document.getElementById('dia_ini').value = document.getElementById('dataini').value ;
        document.getElementById('dia_fim').value = document.getElementById('datafim').value ;
    {rdelim}
</script>
