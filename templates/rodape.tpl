{*
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 *}
<table class="rodape">
   <tr style="border:none !important;">
      <td width="50%" class="subtable" align="left"  style="border:none !important;">
         <b>{$EMP_NOME}</b>
      </td>
      <td width="50%" class="subtable" align="right"  style="border:none !important;">
          SNEP, {$i18n->translate("Versão")}: {$VERSAO}
      </td>
   </tr>
</table>
 
<!-- Fecha TAG'S abertas em cabecalho.tpl -->
</div>  <!-- Fecha o div id=container -->
</body>
</html>