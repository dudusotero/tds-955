#!/bin/bash 
# -----------------------------------------------------------------------------
# Programa: snep_backup - Faz copia do /etc/ e do banco de dados do snep
# Copyright (c) 2008 - Opens Tecnologia - Projeto SNEP
# Licenciado sob Creative Commons. Veja arquivo ./doc/licenca.txt
# Autor: Flavio Henrique Somensi <flavio@opens.com.br>
# Comentarios: Atencao para a definicao do usuario e senha de acesso ao Mysql
# 
# A execucao pode estar agendada no cron (/var/spool/cron/crontabs/root)
# Exemplo: 
# 59 20 * * * /var/www/snep/scripts/snep_backup.sh
# -----------------------------------------------------------------------------
# Backup de Servidores SNEP - Diretorios de sistema e configuracoes
# -----------------------------------------------------------------------------
BACKUP_DIR=/home/backup/          # Diretorio onde serão gravados os backups
BACKUP_NUM=15                     # Numero de backups do rodizio
pastas="etc snep"                 # pastas que serão feitos os backups

diretorio[0]=/etc                 # Diretorio                 
excludes[0]=""                    # Arquivo/diretorio excluido

diretorio[1]=/var/www/snep
excludes[1]=arquivos

# Backup das Pastas
# -----------------------------------------------------------------------------
i=0
for d in $pastas; do
   if [ ! -d $BACKUP_DIR/$d ]; then
      echo -n "    Criando pasta $BACKUP_DIR/$d.. "
      mkdir -p $BACKUP_DIR/$d
      echo "OK"
   fi
   archive=$BACKUP_DIR/$d/$d.tgz
   if [ -f $archive.$BACKUP_NUM ]; then
      rm -f $archive.$BACKUP_NUM;
   fi
   n=$(( $BACKUP_NUM - 1 ))
   while [ $n -gt 0 ]; do
      if [ -f $archive.$n ]; then
         mv $archive.$n $archive.$(( $n + 1 ))
      fi
      n=$(( $n - 1 ))
   done
   if [ -f $archive ]; then
      mv $archive $archive.1;
   fi
   echo -n "Fazendo backup $d... "
   echo ${diretorio[$i]}
   if [ "${excludes[$i]}" == "" ] ; then
      tar cvzfh  $archive ${diretorio[$i]} 
   else
      tar cvzfh  $archive ${diretorio[$i]} --exclude "${excludes[$i]}"
   fi
   chmod 777 $archive
   echo "saindo."
   i=$((++i))
done   
#--------------------------------------------------------------------------
# Backup SNEP - Banco de Dados
#--------------------------------------------------------------------------
BACKUP_DIR=$BACKUP_DIR/mysql
USUARIO=root
PASSWORD="sneppass"

if [ "$PASSWORD" != "" ]; then
   databases=`mysql -u$USUARIO -p$PASSWORD -e"show databases"`
else
   databases=`mysql -u$USUARIO -e"show databases"`
fi


for d in $databases; do
if [ $d != "Database" ]; then
    echo -n "*** Extraindo $d... "
    if [ ! -d $BACKUP_DIR/$d ]; then
       echo -n "    Criando pasta $BACKUP_DIR/$d... "
       mkdir -p $BACKUP_DIR/$d
       echo "OK"
    fi
    archive=$BACKUP_DIR/$d/$d.gz
    if [ -f $archive.$BACKUP_NUM ]; then
       rm -f $archive.$BACKUP_NUM;
    fi
    n=$(( $BACKUP_NUM - 1 ))
    while [ $n -gt 0 ]; do
       if [ -f $archive.$n ]; then
          mv $archive.$n $archive.$(( $n + 1 ))
       fi
       n=$(( $n - 1 ))
    done
    if [ -f $archive ]; then
       mv $archive $archive.1;
    fi
    echo -n "Fazendo database $d... "
    if [ "$PASSWORD" != "" ]; then
       (mysqldump -u$USUARIO -p$PASSWORD  $d | gzip --best) > $archive
    else
       (mysqldump -u$USUARIO $d | gzip --best) > $archive
    fi

    chmod 777 $archive

    echo "saindo."
fi
done

