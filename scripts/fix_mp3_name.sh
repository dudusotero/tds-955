#!/bin/bash


function fix_mp3_name() {
    DIR=$1

    if [ ! -d $DIR ]; then
        echo "Diretório inválido $DIR"
        return
    fi

    if [ -z "$DIR" ]; then
        echo "Diretório não informado"
        return
    fi

    cd $1
    echo $(pwd)

    found=$(find . -maxdepth 1 -iname "*.wav.mp3")
    if [ -z "$found"  ]; then
        echo "Nenhum arquivo '.wav.mp3' para converter'"
        return
    fi

    for file in *.wav.mp3; do
        mv "$file" "`basename $file .wav.mp3`.mp3"
    done
}

for dir in $(ls -d $1/*); 
do
    echo "Fix in $dir"
    fix_mp3_name $dir
done
