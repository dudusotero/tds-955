#!/bin/bash 
# -----------------------------------------------------------------------------
# Programa: prepare_file_counter - Prepara arquivo file_counter para movimentacao
# de grande quantidade de arquivos
# Copyright (c) 2014 - Opens Tecnologia - Projeto SNEP
# Licenciado sob Creative Commons. Veja arquivo ./doc/licenca.txt
# Autor: Eduardo Nunes Pereira <eduardo.pereira@opens.com.br>
# Autor: Flavio Henrique Somensi <flavio@opens.com.br>
# Comentarios: - le todos os arquivos com extensao wav que estao
#              - dentro de /var/www/snep/arquivo
#              - escreve o nome de todos os arquivos em file_counter, que sera
#                usado posteriormente pelo movefiles_partials.sh
#              - move somente para um storage
#
# A execucao pode estar agendada no cron (/var/spool/cron/crontabs/root)
# Exemplo: 
# 59 23 * * * /var/www/snep/scripts/prepare_file_counter.sh
# -----------------------------------------------------------------------------

CAT=$(which  cat)
CUT=$(which cut)
MP3=`${CAT} /var/www/snep/includes/setup.conf | grep record_mp3 | ${CUT} -d'=' -f 2 | tr -d ' ' | tr -d '"'`
file_counter=/var/www/snep/arquivos/file_counter.log
ALREADY_MOVED=false

# Funcao que move arquivos para storage
mover() {
    cd $1
    dst=$2

    if [ -f $file_counter ]; then
        rm $file_counter;
    fi

    ls -ltr --time-style=long-iso | grep -E '^*.WAV|wav$' | while read perms o owner group size date time file 
    do
        if [ ! -f $file_counter ]; then
            touch $file_counter
        fi
    
        file=$(echo $file | basename $file)
        found=$(grep "$date;$file" $file_counter)
        if [ $? != 0 ]; then 
            echo "$date;$file" >> $file_counter
        fi
    done

    if [ -f $file_counter ]; then
        cp $file_counter $file_counter.bk
        tac $file_counter.bk > $file_counter
        rm $file_counter.bk
    fi
}

echo "File counter inicio $(date)" >> /var/log/snep/mover_parcial.log

# verifica discos montados em como storage {storage1, storage2}
echo | df -P -h | grep storage | awk '{print $6 ":"$5}' > storages.swp
#verifica espoaço disponivel onde esta montado /var/www/snep/arquivos
echo | df -P /var/www/snep/arquivos | tail -1 | awk '{print $6 ":"$5}' >> storages.swp

# percorre cada um dos registros encontrados
for i in `cat storages.swp ` ; do

    # retorna posicao de caracteries dentro da string ':' e '%'
    if [ -n $i ]; then
    	xx=`echo | expr index $i :`
	xy=`echo | expr index $i %`
        
	# corta strings
        string=${i:0:$xy-1}
        mount=${string:0:$xx-1}
        porcent=${string:$xx:$xy}
        host=$(cat /etc/hostname )

        if [ $porcent -lt "97" ]; then
            if [ $ALREADY_MOVED == true ]; then
                break;
            fi

            mover "/var/www/snep/arquivos" $mount
            ALREADY_MOVED=true
        else
            echo "Storage perto do limite de espaco | mail -s "Storage perto do limite de espaço em $host " suporte-snep@opens.com.br"
	    echo "Storage: " $mount "com espaco insuficiente."
            break
        fi
    else
        echo "Nenhum Storage encontrado | mail -s  "Nenhum Storage encontrado em $host "  rafael@opens.com.br"
	echo "Nenhum Storage montado em /var/www/snep/arquivos/"
    fi

done

echo "File counter fim $(date)" >> /var/log/snep/mover_parcial.log

if [ -f $file_counter ]; then
   bash /var/www/snep/scripts/movefiles_partials.sh
fi
