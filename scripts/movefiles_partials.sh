#!/bin/bash 
# -----------------------------------------------------------------------------
# Programa: movefiles_partials - Move Arquivos de gravacao para diretorio especifico, 
# utilizado para mover graduativamente de forma diaria, grande quantidade de arquivos
# Copyright (c) 2014 - Opens Tecnologia - Projeto SNEP
# Licenciado sob Creative Commons. Veja arquivo ./doc/licenca.txt
# Autor: Eduardo Nunes Pereira <eduardo.pereira@opens.com.br>
# Autor: Flavio Henrique Somensi <flavio@opens.com.br>
# Comentarios: - le todos os arquivos da lista file_counter.log
#              - a cada execucao move os arquivos da lista e remove da lista os
#                arquivos movidos 
#              - conta somente um storage
#              - cria um sub-diretorio para cada data encontrada
#              - move os arquivos para seus respectivos sub-diretorios
#              - Se estiver definido, converte arquivos para MP3
#                ( Define-se na interface: Configuracoes >> Parametros)
#              _ Tenta encontrar outro disco montado em: 
#                /var/www/snep/arquivos/storage
#
# A execucao pode estar agendada no cron (/var/spool/cron/crontabs/root)
# Exemplo: 
# 59 23 * * * /var/www/snep/scripts/movefiles_partials.sh
# -----------------------------------------------------------------------------

CAT=$(which  cat)
CUT=$(which cut)
MP3=`${CAT} /var/www/snep/includes/setup.conf | grep record_mp3 | ${CUT} -d'=' -f 2 | tr -d ' ' | tr -d '"'`
PARTIALS_PER_NIGHT=10
ALREADY_MOVED=false

mover_parciais() {
    cd $1
    dst=$2

    file_counter=/var/www/snep/arquivos/file_counter.log
    if [ ! -f $file_counter ]; then
        echo "Erro: arquivo file_counter.log nao encontrado, veja prepare_file_counter.sh"
        echo "Erro: arquivo file_counter.log nao encontrado, veja prepare_file_counter.sh" >> /var/log/snep/mover_parcial.log
        exit 1
    fi

    for date_file in $(head -n $PARTIALS_PER_NIGHT $file_counter);
    do
        IFS=';' read -ra ADDR <<< "$date_file"

        file=${ADDR[1]}
        date=${ADDR[0]}

        if [ -f $file ]; then
            if [ ! -d $dst"/"$date ];then
                echo -en "Criando diretorio $date..."
                mkdir  $dst"/"$date
            fi

            echo "Arquivo movido $(date) : $file" >> /var/log/snep/mover_parcial.log

            if [ "$MP3" == "true" ];then
                no_sufix_name=$(basename $file .wav)
                no_sufix_name=$(basename $file .WAV)

                lame -b 32 $file $dst"/"$date"/"$no_sufix_name.mp3
                arquivo=$dst"/"$date"/"$no_sufix_name.mp3

                if [ ! -f $arquivo ];then
                    sox $file -t wav -s - | lame -b 32 - $dst"/"$date"/"$no_sufix_name.mp3
                fi

                chown www-data.www-data $arquivo

                siz=`du $arquivo |cut -f1`
                if [ -f $arquivo ];then
                    if [ $siz != "0" ];then
                        # Diretorio utilizado para garantir caso procedimento de errado
                        if [ ! -d /var/www/snep/arquivos/backup_audios ]; then
                            mkdir /var/www/snep/arquivos/backup_audios
                        fi

                        mv $file /var/www/snep/arquivos/backup_audios
                    fi
                fi
            else
                # Procedimento normal sem conversao para MP3
                chown www-data.www-data $file
                mv $file $dst"/"$date"/"
            fi
        fi
    done

    sed -i "1,$PARTIALS_PER_NIGHT d" $file_counter
    ALREADY_MOVED=true
}

echo "Mover parcial inicio $(date)" >> /var/log/snep/mover_parcial.log

# verifica discos montados em como storage {storage1, storage2}
echo | df -P -h | grep storage | awk '{print $6 ":"$5}' > storages.swp
#verifica espoaço disponivel onde esta montado /var/www/snep/arquivos
echo | df -P /var/www/snep/arquivos | tail -1 | awk '{print $6 ":"$5}' >> storages.swp

# percorre cada um dos registros encontrados
for i in `cat storages.swp ` ; do

    # retorna posicao de caracteries dentro da string ':' e '%'
    if [ -n $i ]; then
    	xx=`echo | expr index $i :`
	xy=`echo | expr index $i %`
        
	# corta strings
        string=${i:0:$xy-1}
        mount=${string:0:$xx-1}
        porcent=${string:$xx:$xy}
        host=$(cat /etc/hostname )

        if [ $porcent -lt "97" ]; then
            if [ $ALREADY_MOVED == true ]; then
                break;
            fi

            mover_parciais "/var/www/snep/arquivos" $mount
            ALREADY_MOVED=true
        else
            echo "Storage perto do limite de espaco | mail -s "Storage perto do limite de espaço em $host " suporte-snep@opens.com.br"
    	    echo "Storage: " $mount "com espaco insuficiente."
            break
        fi
    else
        echo "Nenhum Storage encontrado | mail -s  "Nenhum Storage encontrado em $host "  rafael@opens.com.br"
	echo "Nenhum Storage montado em /var/www/snep/arquivos/"
    fi

done

echo "Mover parcial fim $(date)" >> /var/log/snep/mover_parcial.log

bash /var/www/snep/scripts/fix_mp3_name.sh /var/www/snep/arquivos/storage1/
