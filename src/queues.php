<?php

/**
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Classe queues Implementa as funcionalidades da fila.
 *
 * @category  Snep
 * @package   src_queues
 * @copyright Copyright (c) 2010 OpenS Tecnologia
 * @author Opens Tecnologia
 */
require_once("../includes/verifica.php");
require_once("../configs/config.php");

ver_permissao(19);

// Faz Leitura do Arquivo snep-musiconhold.conf
$row = executacmd("cat /etc/asterisk/snep/snep-musiconhold.conf", "", True);
$secoes = array("" => "");
$secao = "";

foreach ($row as $key => $value) {
    if ((substr($value, 0, 1) === ";" &&
            substr($value, 1, 4) != "SNEP") ||
            substr($value, 0, 1) == "[" ||
            strlen(trim($value)) == 0)
        continue;

    if (substr($value, 0, 5) == ';SNEP') {
        $secao = substr($value, 6);
        $secao = substr($secao, 0, strpos($secao, ")"));
        $secoes[$secao] = $secao . " (" . substr($value, strpos($value, "=") + 1, 30) . "...)";
        continue;
    }
}
// Sons que estao no diretorio de sons
$sounds = array("" => "");
$files = scandir(SNEP_PATH_SOUNDS);

foreach ($files as $i => $value) {
    if (substr($value, 0, 1) == '.') {
        unset($files[$i]);
        continue;
    }
    if (is_dir(SNEP_PATH_SOUNDS . $value)) {
        unset($files[$i]);
        continue;
    }
    $name = substr($value, 0, strpos($value, '.'));
    $sounds[$name] = $value;
}
    
// Variaveis de ambiente do form
$tipos_holdtime = array("yes" => $LANG['yes'],
    "no" => $LANG['no'],
    "once" => $LANG['once']);

$tipos_joinempty = array("yes" => $LANG['yes'],
    "no" => $LANG['no'],
    "strict" => $LANG['strict']);

$tipos_strategy = array("ringall" => $LANG['ringall'] . " (ringall)",
    "roundrobin" => $LANG['roundrobin'] . " (roundrobin)",
    "leastrecent" => $LANG['leastrecent'] . " (lastrecent)",
    "random" => $LANG['random'] . " (random)",
    "fewestcalls" => $LANG['fewestcalls'] . " (fewestcalls)",
    "rrmemory" => $LANG['rrmemory'] . " (rrmemory)");

$smarty->assign('ACAO', $acao);
$smarty->assign('OPCOES_HOLDTIME', $tipos_holdtime);
$smarty->assign('OPCOES_JOINEMPTY', $tipos_joinempty);
$smarty->assign('OPCOES_STRATEGY', $tipos_strategy);
$smarty->assign('OPCOES_TRUEFALSE', $tipos_tf);
$smarty->assign('OPCOES_SECAO', $secoes);
$smarty->assign('OPCOES_SONS', $sounds);
$smarty->assign('SOUNDS_PATH', SNEP_PATH_SOUNDS);
$smarty->assign('PROTOTYPE', true);

if ($acao == "cadastrar") {
    cadastrar();
} elseif ($acao == "alterar") {
    $titulo = $LANG['menu_register'] . " » " .
            $LANG['menu_queues'] . " » " .
            $LANG['change'];
    alterar();
} elseif ($acao == "grava_alterar") {
    grava_alterar();
} elseif ($acao == "excluir") {
    excluir();
} else {
    $titulo = $LANG['menu_register'] . " » " .
            $LANG['menu_queues'] . " » " .
            $LANG['include'];
    principal();
}

/**
 * principal - Monta a tela principal da rotina
 * @global <object> $smarty
 * @global <String> $titulo
 * @global <array> $SETUP
 */
function principal() {

    global $smarty, $titulo, $SETUP;

    $alert_visual['ativo'] = 0;
    $alert_sonoro['ativo'] = 0;
    $alert_email['ativo'] = 0;
    $dt_queues['max_time_call'] = $SETUP['ambiente']['max_time_call'];
    $dt_queues['max_call_queue'] = $SETUP['ambiente']['max_call_queue'];

    $smarty->assign('ACAO', "cadastrar");
    $smarty->assign('alert_visual', $alert_visual);
    $smarty->assign('alert_sonoro', $alert_sonoro);
    $smarty->assign('alert_email', $alert_email);
    $smarty->assign('dt_queues', $dt_queues);
    display_template("queues.tpl", $smarty, $titulo);
}

/**
 * cadastrar - Inclui uma nova fila no sistema
 * @global <array> $LANG
 * @global <object> $db
 * @global <string> $musiconhold - Classe de musica em espera
 * @global <string> $announce - Arquivo de som para anúncio da chamada ao agente após atendimento
 * @global <string> $context - Contexto de desvio de chamada
 * @global <string> $timeout - Tempo de toque em cada agente
 * @global <boolean> $monitor_type
 * @global <string> $monitor_format
 * @global <string> $queue_youarenext - Arquivo de Mensagem: Voce é o proximo da fila
 * @global <string> $queue_thereare - Arquivo de Mensagem: Voce esta aqu
 * @global <string> $queue_callswaiting - Arquivo de Mensagem: Existem 'n' Chamadas aguardando
 * @global <string> $queue_holdtime
 * @global <string> $queue_minutes
 * @global <string> $queue_seconds
 * @global <string> $queue_lessthan
 * @global <string> $queue_thankyou - Arquivo de Mensagem: Obrigado por aguardar
 * @global <string> $queue_reporthold
 * @global <string> $announce_frequency - Intervalo de repetição das mensagens ao chamados
 * @global <string> $announce_round_seconds
 * @global <string> $announce_holdtime
 * @global <string> $retry - Tempo de espera para tentar chamar os Agentes novamente
 * @global <string> $wrapuptime - Tempo de descanso do agente entre uma chamada e outra
 * @global <string> $maxlen - Numero maximo de chamadas em espera na fila
 * @global <string> $servicelevel - Nível de Serviço da Fila
 * @global <string> $strategy - Estratégia de distribuição das chamadas
 * @global <boolean> $joinempty - Usuários podem entrar na Fila mesmo sem Agentes presentes?
 * @global <boolean> $leavewhenempty - Chamadas devem sair da fila quando os Agentes sairem?
 * @global <string> $eventmemberstatus
 * @global <boolean> $eventwhencalled
 * @global <boolean> $reportholdtime - Avisar ao Agente o tempo que a chamada esta esperando na fila
 * @global <string> $memberdelay - Tempo de descanso para o Agente
 * @global <string> $weight - Prioridade da fila
 * @global <string> $periodic_announce
 * @global <int> $periodic_announce_frequency
 * @global <string> $max_call_queue
 * @global <string> $max_time_call
 * @global <string> $alert_mail
 */
function cadastrar() {
    global $LANG, $db, $musiconhold, $announce, $context, $timeout, $monitor_type, $monitor_format, $queue_youarenext, $queue_thereare, $queue_callswaiting, $queue_holdtime, $queue_minutes, $queue_seconds, $queue_lessthan, $queue_thankyou, $queue_reporthold, $announce_frequency, $announce_round_seconds, $announce_holdtime, $retry, $wrapuptime, $maxlen, $servicelevel, $strategy, $joinempty, $leavewhenempty, $eventmemberstatus, $eventwhencalled, $reportholdtime, $memberdelay, $weight, $periodic_announce, $periodic_announce_frequency, $max_call_queue, $max_time_call, $alert_mail;

    $name = $_POST['name'];
    $msg = validaNome($name);
    if($msg == true){
        display_error($LANG['error'] . 'Fila já existe. Modifique o nome!', true);
    }

    // Campos Default
    $eventwhencalled = True;
    $monitor_type = False;
    $monitor_format = "";
    $timeoutrestart = "";

    //Desabilitados
    $announce_round_seconds = 0;
    $periodic_announce_frequency = 0;


    Snep_Alertas::setAlerta($name, array('tipo' => 'email',
        'tme' => $_POST['a_email_tme'],
        'sla' => $_POST['a_email_sla'],
        'item' => $name,
        'alerta' => 'alerta',
        'destino' => $_POST['a_email_emails'],
        'ativo' => $_POST['a_email_ativo']));

    Snep_Alertas::setAlerta($name, array('tipo' => 'sonoro',
        'tme' => $_POST['a_sonoro_tme'],
        'sla' => $_POST['a_sonoro_sla'],
        'item' => $name,
        'alerta' => 'alerta',
        'destino' => 'tela',
        'ativo' => $_POST['a_sonoro_ativo']));

    Snep_Alertas::setAlerta($name, array('tipo' => 'visual',
        'tme' => $_POST['a_visual_tme'],
        'sla' => $_POST['a_visual_sla'],
        'item' => $name,
        'alerta' => 'alerta',
        'destino' => 'tela',
        'ativo' => $_POST['a_visual_ativo']));
    

    $sql = "INSERT INTO queues ";
    $sql .= " VALUES ('$name', '$musiconhold', '$announce', '$context', $timeout, '$monitor_type', '$monitor_format', '$queue_youarenext', '$queue_thereare', '$queue_callswaiting', '$queue_holdtime', '$queue_minutes', '$queue_seconds', '$queue_lessthan', '$queue_thankyou', '$queue_reporthold', $announce_frequency, $announce_round_seconds, '$announce_holdtime', $retry, $wrapuptime, $maxlen, $servicelevel, '$strategy', '$joinempty', '$leavewhenempty', '$eventmemberstatus', '$eventwhencalled', '$reportholdtime', $memberdelay, $weight, '$timeoutrestart', '$periodic_announce', $periodic_announce_frequency,'0','0','$alert_mail')";

    try {
        $db->beginTransaction();
        $db->exec($sql);
        $db->commit();

        $tabela = "";
        $tabela = verificaLog($tabela);
        if ($tabela == true) {
            //log
            $id = $name;
            $acao = "Adicionou Fila";
            salvaLog($acao, $id);

            $action = "ADD";
            $add = getQueue($id);
            insertLogQueue($action, $add);
        }

        // Executa comando do Asterisk para recarregar as Filas
        ast_status("module reload app_queue.so", "");
        ast_status("queues show", ""); // Compatibilidade com ramais SIP no novo Painel
        echo "<meta http-equiv='refresh' content='0;url=../index.php/queues'>\n";
    } catch (Exception $e) {
        $db->rollBack();
        display_error($LANG['error'] . $e->getMessage(), true);
    }
}

/**
 * alterar - Altera a fila selecionada
 * @global <array> $LANG
 * @global <object> $db
 * @global <object> $smarty
 * @global <string> $titulo
 * @global <string> $acao 
 */
function alterar() {

    global $LANG, $db, $smarty, $titulo, $acao;

    $name = ( isset($_POST['name']) ? $_POST['name'] : $_GET['name'] );
    if (!$name) {
        display_error($LANG['msg_notselect'], true);
        exit;
    }

    $alertas = Snep_Alertas::getAlertas($name);
    $arr_Alertas = array();


    foreach ($alertas as $id => $alerta) {
        $arr_Alertas[$alerta['tipo']] = $alerta;
    }

    try {
        $sql = "SELECT * FROM queues WHERE name='" . mysql_escape_string($name) . "'";
        $row = $db->query($sql)->fetch();
    } catch (PDOException $e) {
        display_error($LANG['error'] . $e->getMessage(), true);
    }

    if (strpos($row['announce'], '.') > 1) {
        $row['announce'] = substr($row['announce'], 0, strpos($row['announce'], '.'));
    }
    if (strpos($row['queue_youarenext'], '.') > 1) {
        $row['queue_youarenext'] = substr($row['queue_youarenext'], 0, strpos($row['queue_youarenext'], '.'));
    }
    if (strpos($row['queue_thereare'], '.') > 1) {
        $row['queue_thereare'] = substr($row['queue_thereare'], 0, strpos($row['queue_thereare'], '.'));
    }
    if (strpos($row['queue_callswaiting'], '.') > 1) {
        $row['queue_callswaiting'] = substr($row['queue_callswaiting'], 0, strpos($row['queue_callswaiting'], '.'));
    }
    if (strpos($row['queue_thankyou'], '.') > 1) {
        $row['queue_thankyou'] = substr($row['queue_thankyou'], 0, strpos($row['queue_thankyou'], '.'));
    }

    $smarty->assign('alert_sonoro', ( isset($arr_Alertas['sonoro']) ? $arr_Alertas['sonoro'] : ''));
    $smarty->assign('alert_visual', ( isset($arr_Alertas['visual']) ? $arr_Alertas['visual'] : ''));
    $smarty->assign('alert_email', ( isset($arr_Alertas['email']) ? $arr_Alertas['email'] : ''));
    $smarty->assign('ACAO', "grava_alterar");
    $smarty->assign('dt_queues', $row);
    display_template("queues.tpl", $smarty, $titulo);
}

/**
 * grava_alterar - Grava o registro alterado
 * @global <array> $LANG
 * @global <object> $db
 * @global <string> $name
 * @global <string> $musiconhold - Classe de musica em espera
 * @global <string> $announce - Arquivo de som para anúncio da chamada ao agente após atendimento
 * @global <string> $context - Contexto de desvio de chamada
 * @global <string> $timeout - Tempo de toque em cada agente
 * @global <boolean> $monitor_type
 * @global <string> $monitor_format
 * @global <string> $queue_youarenext - Arquivo de Mensagem: Voce é o proximo da fila
 * @global <string> $queue_thereare - Arquivo de Mensagem: Voce esta aqu
 * @global <string> $queue_callswaiting - Arquivo de Mensagem: Existem 'n' Chamadas aguardando
 * @global <string> $queue_holdtime
 * @global <string> $queue_minutes
 * @global <string> $queue_seconds
 * @global <string> $queue_lessthan
 * @global <string> $queue_thankyou - Arquivo de Mensagem: Obrigado por aguardar
 * @global <string> $queue_reporthold
 * @global <string> $announce_frequency - Intervalo de repetição das mensagens ao chamados
 * @global <string> $announce_round_seconds
 * @global <string> $announce_holdtime
 * @global <string> $retry - Tempo de espera para tentar chamar os Agentes novamente
 * @global <string> $wrapuptime - Tempo de descanso do agente entre uma chamada e outra
 * @global <string> $maxlen - Numero maximo de chamadas em espera na fila
 * @global <string> $servicelevel - Nível de Serviço da Fila
 * @global <string> $strategy - Estratégia de distribuição das chamadas
 * @global <boolean> $joinempty - Usuários podem entrar na Fila mesmo sem Agentes presentes?
 * @global <boolean> $leavewhenempty - Chamadas devem sair da fila quando os Agentes sairem?
 * @global <string> $eventmemberstatus
 * @global <boolean> $eventwhencalled
 * @global <boolean> $reportholdtime - Avisar ao Agente o tempo que a chamada esta esperando na fila
 * @global <string> $memberdelay - Tempo de descanso para o Agente
 * @global <string> $weight - Prioridade da fila
 * @global <string> $periodic_announce
 * @global <int> $periodic_announce_frequency
 * @global <string> $max_call_queue
 * @global <string> $max_time_call
 * @global <string> $alert_mail
 */
function grava_alterar() {

    global $LANG, $db, $name, $musiconhold, $announce, $context, $timeout, $monitor_type, $monitor_format, $queue_youarenext, $queue_thereare, $queue_callswaiting, $queue_holdtime, $queue_minutes, $queue_seconds, $queue_lessthan, $queue_thankyou, $queue_reporthold, $announce_frequency, $announce_round_seconds, $announce_holdtime, $retry, $wrapuptime, $maxlen, $servicelevel, $strategy, $joinempty, $leavewhenempty, $eventmemberstatus, $eventwhencalled, $reportholdtime, $memberdelay, $weight, $periodic_announce, $periodic_announce_frequency, $max_call_queue, $max_time_call, $alert_mail;

    $name = $_POST['name'];

    // Limpa alertas desta fila e os recria.
    Snep_Alertas::resetAlertas($name);

    Snep_Alertas::setAlerta($name, array('tipo' => 'email',
        'tme' => $_POST['a_email_tme'],
        'sla' => $_POST['a_email_sla'],
        'item' => $name,
        'alerta' => 'alerta',
        'destino' => $_POST['a_email_emails'],
        'ativo' => $_POST['a_email_ativo']));

    Snep_Alertas::setAlerta($name, array('tipo' => 'sonoro',
        'tme' => $_POST['a_sonoro_tme'],
        'sla' => $_POST['a_sonoro_sla'],
        'item' => $name,
        'alerta' => 'alerta',
        'destino' => 'tela',
        'ativo' => $_POST['a_sonoro_ativo']));

    Snep_Alertas::setAlerta($name, array('tipo' => 'visual',
        'tme' => $_POST['a_visual_tme'],
        'sla' => $_POST['a_visual_sla'],
        'item' => $name,
        'alerta' => 'alerta',
        'destino' => 'tela',
        'ativo' => $_POST['a_visual_ativo']));

    // Campos desabilitados
    $announce_round_seconds = 0;
    $periodic_announce_frequency = 0;
    if (strpos($row['announce'], '.') > 1) {
        $announce = substr($announce, 0, strpos($announce, "."));
    }

    $sql = " UPDATE queues SET ";
    $sql.= " musiconhold='$musiconhold', announce='$announce', context='$context', ";
    $sql.= " timeout=$timeout, monitor_type='$monitor_type', monitor_format='$monitor_format', ";
    $sql.= " queue_youarenext='$queue_youarenext', queue_thereare='$queue_thereare', ";
    $sql.= " queue_callswaiting='$queue_callswaiting', queue_holdtime='$queue_holdtime', ";
    $sql .= " queue_minutes='$queue_minutes', queue_seconds='$queue_seconds', queue_lessthan='$queue_lessthan', ";
    $sql .= " queue_thankyou='$queue_thankyou', queue_reporthold='$queue_reporthold', ";
    $sql .= " announce_frequency=$announce_frequency, announce_round_seconds=$announce_round_seconds, ";
    $sql .= " announce_holdtime='$announce_holdtime', retry=$retry, wrapuptime=$wrapuptime, maxlen=$maxlen, ";
    $sql .= " servicelevel=$servicelevel, strategy='$strategy', joinempty='$joinempty', ";
    $sql .= " leavewhenempty='$leavewhenempty', eventmemberstatus='$eventmemberstatus', ";
    $sql .= " eventwhencalled='$eventwhencalled', reportholdtime='$reportholdtime', memberdelay=$memberdelay, ";
    $sql .= " weight=$weight,  periodic_announce='$periodic_announce', ";
    $sql .= " periodic_announce_frequency=$periodic_announce_frequency ";
    $sql .= " WHERE name='$name' ";

    try {

        $db->beginTransaction();
        $db->exec($sql);
        $db->commit();
        // Executa comando do Asterisk para recarregar as Filas
        ast_status("module reload app_queue.so", "");
        ast_status("queues show", ""); // Compatibilidade com ramais SIP no novo Painel
        echo "<meta http-equiv='refresh' content='0;url=../index.php/queues'>\n";
    } catch (Exception $e) {
        $db->rollBack();
        display_error($LANG['error'] . $e->getMessage(), true);
    }
}

/**
 * excluir - Exclui registro selecionado
 * @global <array> $LANG
 * @global <object> $db
 */
function excluir() {
    global $LANG, $db;


    $name = isset($_POST['name']) ? $_POST['name'] : $_GET['name'];

    if (!$name) {
        display_error($LANG['msg_notselect'], true);
        exit;
    }

    Snep_Alertas::resetAlertas($name);

    try {
        // Procurando por conflito com regras de negócio
        $rules_query = "SELECT id, `desc` FROM regras_negocio WHERE origem LIKE '%T:$name%' OR destino LIKE '%T:$name%'";
        $regras = $db->query($rules_query)->fetchAll();

        $rules_query = "SELECT rule.id, rule.desc FROM regras_negocio as rule, regras_negocio_actions_config as rconf WHERE (rconf.regra_id = rule.id AND rconf.value = '$name' AND (rconf.key = 'queue'))";
        $valida = 0;
        foreach ($db->query($rules_query)->fetchAll() as $rule) {
            if (!in_array($rule, $regras)) {
                $regras[] = $rule;
            }
        }

        if (count($regras) > 0) {

            $msg = $LANG['queue_conflict_in_rules'] . ":<br />\n";
            foreach ($regras as $regra) {
                $msg .= $regra['id'] . " - " . $regra['desc'] . "<br />\n";
                display_error($msg, true);
                exit(1);
            }
        }

        $msg = $LANG['members_conflict_in_rules'] . ":<br />\n";

        // Procurando por Ramais estáticos na fila
        $exten_member = "SELECT `membername` FROM `queue_members` WHERE `queue_name` = '$name'";
        $exten_members = $db->query($exten_member)->fetchAll();

        // Procurando por Agentes na fila
        $agent_member = "SELECT `agent_id` FROM `queues_agent` WHERE `queue` = '$name'";
        $agent_members = $db->query($agent_member)->fetchAll();

        // Se caso possua ramais estáticos na fila
        if (count($exten_members) > 0) {
            $valida = 1;

            foreach ($exten_members as $membros) {
                $member = explode("/", $membros['membername']);
                $member = $member[1];
                $msg .= "Ramal:" . $member . "<br/>\n";
            }
        }

        // Se caso possua agentes na fila
        if (count($agent_members) > 0) {
            $valida = 1;
            foreach ($agent_members as $member_agent) {

                $msg .= "Agente:" . $member_agent['agent_id'] . "<br/>\n";
            }
        }

        // Emite mensagem ao usuário sobre ramais ou agentes na fila
        if ($valida == 1) {
            display_error($msg, true);
            exit(1);
        }

        $queue_peers = "DELETE FROM queue_peers WHERE fila='" . mysql_escape_string($name) . "'";
        $db->beginTransaction();
        $db->exec($queue_peers);
        $db->commit();

        $tabela = "";
        $tabela = verificaLog($tabela);
        if ($tabela == true) {
            //log
            $id = $name;
            $add = getQueue($id);
        }

        $sql = "DELETE FROM queues WHERE name='" . mysql_escape_string($name) . "'";
        $db->beginTransaction();
        $db->exec($sql);
        $db->commit();


        if ($tabela == true) {
            //log
            $acao = "Excluiu Fila";
            salvaLog($acao, $id);
            $action = "DEL";
            insertLogQueue($action, $add);
        }

        // Executa comando do Asterisk para recarregar as Filas
        ast_status("module reload app_queue.so", "");
        ast_status("queues show", ""); // Compatibilidade com ramais SIP no novo Painel
        // display_error($LANG['msg_excluded'],true) ;
        echo "<meta http-equiv='refresh' content='0;url=../index.php/queues'>\n";
    } catch (PDOException $e) {
        display_error($LANG['error'] . $e->getMessage(), true);
    }
}

/**
 * verificaLog - Verifica se existe módulo Loguser.
 * @param <boolean> $tabela
 * @return <boolean> True ou false
 */
function verificaLog($tabela) {
    if (class_exists("Loguser_Manager")) {
        $tabela = true;
    } else {
        $tabela = false;
    }
    return $tabela;
}

/**
 * salvaLog - Insere dados da ação na tabela logs.
 * @param <String> $ação Ação feita pelo usuário
 * @param <String> $queues id da fila
 * @return <boolean> True ou false
 */
function salvaLog($acao, $queues) {
    $db = Zend_Registry::get("db");
    $ip = $_SERVER['REMOTE_ADDR'];
    $hora = date('Y-m-d H:i:s');
    $tipo = 7;

    $auth = Zend_Auth::getInstance();
    global $id_user;


    $acao = mysql_escape_string($acao);

    $sql = "INSERT INTO `logs` VALUES (NULL, '" . $hora . "', '" . $ip . "', '" . $id_user . "', '" . $acao . "', '" . $queues . "', '" . $tipo . "' , '" . NULL . "', '" . NULL . "', '" . NULL . "', '" . NULL . "')";

    if ($db->query($sql)) {
        return true;
    } else {
        return false;
    }
}

/**
 * getQueue - Monta array com todos dados da fila
 * @param <int> $id - Nome da fila
 * @return <array> $fila - Dados da fila
 */
function getQueue($id) {

    $fila = array();

    $db = Zend_Registry::get("db");
    $sql = "SELECT name, musiconhold, context from  queues where name='$id'";
    $stmt = $db->query($sql);
    $fila = $stmt->fetch();

    return $fila;
}

/**
 * insertLogFila - insere na tabela logs_users os dados das filas
 * @global <int> $id_user
 * @param <array> $add
 */
function insertLogQueue($acao, $add) {

    $db = Zend_Registry::get("db");
    $ip = $_SERVER['REMOTE_ADDR'];
    $hora = date('Y-m-d H:i:s');

    $auth = Zend_Auth::getInstance();
    global $id_user;

    $select = "SELECT name from peers where id = '$id_user'";
    $stmt = $db->query($select);
    $id = $stmt->fetch();

    $sql = "INSERT INTO `logs_users` VALUES (NULL, '" . $hora . "', '" . $ip . "', '" . $id["name"] . "', '" . $add["name"] . "', '" . $add["musiconhold"] . "', '" . $add["context"] . "', '" . NULL . "', '" . "Fila" . "', '" . $acao . "')";
    $db->query($sql);
}

function validaNome($id) {

    $db = Zend_Registry::get("db");
    $select = "SELECT name from queues where name = '$id'";
    $stmt = $db->query($select);
    $value = $stmt->fetch();

    if ($value != false) {
        $value = true;
    }

    return($value);
}
