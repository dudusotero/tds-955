<?php

/**
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Classe rel_rankings Implementa os filtros para geração do relatório de ranking de ligação
 *
 * @category  Snep
 * @package   src_rel_chamadas
 * @copyright Copyright (c) 2010 OpenS Tecnologia
 * @author Opens Tecnologia
 */

require_once("../includes/verifica.php");
require_once("../configs/config.php");

ver_permissao(29);

global $acao, $prefix_inout;
$prefix_inout = $SETUP['ambiente']['prefix_inout'];

if (!isset($csv_rel_ranking)) {
    $csv_rel_ranking = '';
}

if (!isset($dst_exceptions)) {
    $dst_exceptions = '';
}

if ($acao == "relatorio" || $acao == "csv") {
    monta_relatorio($acao);
} elseif ($acao == "imp") {
    exibe_relatorio();
}

$dados_iniciais = array(
    "dia_ini" => isset($_SESSION['relrank']['dia_ini']) ? $_SESSION['relrank']['dia_ini'] : "01/" . date('m/Y 00:00'),
    "dia_fim" => isset($_SESSION['relrank']['dia_fim']) ? $_SESSION['relrank']['dia_fim'] : date('d/m/Y H:i'));

for ($i = 1; $i <= 30; $i++) {
    $viewtop[$i] = $i;
}
$titulo = $LANG['menu_reports'] . " » " . $LANG['menu_callranking'];
$smarty->assign('rank_num', ( isset($_SESSION['relrank']['rank_num']) ? $_SESSION['relrank']['rank_num'] : ''));
$smarty->assign('rank_type', ( isset($_SESSION['relrank']['rank_type']) ? $_SESSION['relrank']['rank_type'] : 'qtdade'));
$smarty->assign('viewrec', ( isset($_SESSION['relrank']['viewrec']) ? $_SESSION['relrank']['viewrec'] : 'no'));
$smarty->assign('viewtop', ( isset($_SESSION['relrank']['viewtop']) ? $_SESSION['relrank']['viewtop'] : '10'));
$smarty->assign('OPCOES_REC', $tipos_yn);
$smarty->assign('FILTERS', $dst_exceptions);
$smarty->assign('dt_ranking', $dados_iniciais);
$smarty->assign('VIEWTOP', $viewtop);
$smarty->assign('PROTOTYPE', True);
$smarty->assign('OPCOES_RANK', array("qtdade" => $LANG['rank_qtdade'], "tempo" => $LANG['rank_time']));
display_template("rel_ranking.tpl", $smarty, $titulo);
 
/**
 * monta_relatorio - Monta relatório a ser mostrado 
 */
function monta_relatorio() {

    global $LANG, $SETUP, $db, $smarty, $dia_ini, $dia_fim, $prefix_inout, $dst_exceptions, 
            $filter, $tipos_chamadas, $rank_type, $viewtop, $rank_num, $acao, $viewrec;

    $_SESSION['relrank']['dia_ini'] = $dia_ini;
    $_SESSION['relrank']['dia_fim'] = $dia_fim;
    $_SESSION['relrank']['rank_type'] = $rank_type;
    $_SESSION['relrank']['rank_num'] = $rank_num;
    $_SESSION['relrank']['viewtop'] = $viewtop;
    $_SESSION['relrank']['viewrec'] = $viewrec;

    //---->>>> Primeira clausula do where: periodos inicial e final <<<<----//
    /* Clausula do where: periodos inicial e final  */
    $d = DateTime::createFromFormat("d/m/Y H:i",$dia_ini); 
    if (!$d) {
        display_error($LANG['error'] . $LANG['msg_dateinvalid'] , true);
        exit;
    }
    $date_clause = " ( calldate >= '".$d->format("Y-m-d H:i")."'";
    $d = DateTime::createFromFormat("d/m/Y H:i",$dia_fim); 
    if (!$d) {
        display_error($LANG['error'] . $LANG['msg_dateinvalid'] , true);
        exit;
    }
    $date_clause .=" AND calldate <= '".$d->format("Y-m-d H:i")."' )"; 
    
    $TIT_DATE = $LANG['periodo'] . ": $dia_ini a $dia_fim ";
    $CONDICAO = " WHERE cdr.accountcode != '' AND  cdr.duration > 0 AND $date_clause";

    if (!isset($src)) {
        $src = '';
    }
    if (!isset($dst)) {
        $dst = '';
    }
    if (!isset($orides)) {
        $orides = '';
    }
    if (!isset($srctype)) {
        $srctype = '';
    } if (!isset($dsttype)) {
        $dsttype = '';
    }

    //---->>>> Prefixos de Login/Logout de agentes
    if (strlen($prefix_inout) > 6) {
        $COND_PIO = "";
        $array_prefixo = explode(";", $prefix_inout);

        foreach ($array_prefixo as $valor) {

            $par = explode("/", $valor);
            $pio_in = $par[0];

            if (!isset($par[1])) {
                $pio_out = '';
            }
            else
                $pio_out = $par[1];

            $t_pio_in = strlen($pio_in);
            $t_pio_out = strlen($pio_out);

            $COND_PIO .= " substr(dst,1,$t_pio_in) != '$pio_in' ";
            if (!$pio_out == '') {
                $COND_PIO .= " AND substr(dst,1,$t_pio_out) != '$pio_out' ";
            }
            $COND_PIO .= " AND ";
        }
        if ($COND_PIO != "")
            $CONDICAO .= " AND ( " . substr($COND_PIO, 0, strlen($COND_PIO) - 4) . " ) ";
    }
    //---->>>> Filtro de Descarte

    $TMP_COND = "";
    $dst_exceptions = $SETUP['ambiente']['dst_exceptions'];
    $dst_exceptions = explode(";", $dst_exceptions);
    foreach ($dst_exceptions as $valor) {
        $TMP_COND .= " dst != '$valor' ";
        $TMP_COND .= " AND ";
    }
    $CONDICAO .= " AND ( " . substr($TMP_COND, 0, strlen($TMP_COND) - 4) . " ) ";

    /* Verificando existencia de vinculos no ramal */
    $name = $_SESSION['name_user'];
    $vinculo_table = "";
    $vinculo_where = "";

    if ($name != "admin") {
        $sql = "SELECT id_peer, id_vinculado FROM permissoes_vinculos WHERE id_peer ='$name'";
        $result = $db->query($sql)->fetchObject();
    
        if ($result) {
            $vinculo_table = " ,permissoes_vinculos ";
            $vinculo_where = " AND ( permissoes_vinculos.id_peer='{$result->id_peer}' AND (cdr.src = permissoes_vinculos.id_vinculado OR cdr.dst = permissoes_vinculos.id_vinculado) ) ";
        }
    }

    $CONDICAO .= " AND ( locate('ZOMBIE',channel) = 0 ) ";
    //---->>>> Pegar somente ramais cadastros na tabela peers
    //$CONDICAO .= " AND src IN (SELECT name from peers) " ;
    // Monta SQL da selecao
    $sql = "SELECT cdr.src, cdr.dst, cdr.disposition, cdr.duration,  cdr.userfield, cdr.uniqueid, cdr.accountcode ";
    $sql .= " FROM cdr " . $vinculo_table . $CONDICAO . "  " . $vinculo_where ;
    $sql .= " GROUP BY src,userfield ORDER BY userfield";


    try {

        $userfield = "XXXXXXXXX" ;
        $flag_ini = True; 
        unset($result);
        $dados = array(); 

        foreach ($db->query($sql) as $row) {
            // Verificar se mostra somente rechamadas
            if ($viewrec === 'yes' && strlen($row['src']) < 8) {
                continue ;
            }

            //verificar se existe o módulo cc e tabela cc_configuration no banco
            $configuration = verificaCC();

            if ($configuration != false) {

                $listaAgentes = ListaAgentes();

                //verifica se origem e destino são agentes para renomear
                $srcAgente = $row['src'];
                $dstAgente = $row['dst'];

                foreach ($listaAgentes as $list => $agentes) {

                    if ($agentes["code"] == $srcAgente) {

                        if ($configuration == "ambos") {
                            $srcAgente = $agentes["code"] . " - (" . rtrim($agentes["name"]) . ")";
                        } else if ($configuration == "name") {
                            $srcAgente = rtrim($agentes["name"]);
                        } else {
                            $srcAgente = $agentes["code"];
                        }
                    }

                    if ($agentes["code"] == $dstAgente) {

                        if ($configuration == "ambos") {
                            $dstAgente = $agentes["code"] . " - (" . rtrim($agentes["name"]) . ")";
                        } else if ($configuration == "name") {
                            $dstAgente = rtrim($agentes["name"]);
                        } else {
                            $dstAgente = $agentes["code"];
                        }
                    }
                }

                $row['src'] = $srcAgente;
                $row['dst'] = $dstAgente;
            }

           /* Faz verificacoes para contabilizar valores dentro do mesmo userfield
              So vai contabilziar resultados por userfield */
            if ($userfield != $row['userfield']) {
                if ($flag_ini) {
                    $result[$row['uniqueid']] = $row;
                    $userfield = $row['userfield'];
                    $flag_ini = False;
                    continue;
                }
            } else {
                $result[$row['uniqueid']] = $row;
                continue;
            }

            /* Varre o array da chamada com mesmo userfield                        */
            foreach ($result as $val) {
                // Inicializa todos indices do array
                $src = $val['src']; 
                $dst = $val['dst'];
                if (!isset($dados[$src])) {
                    $dados[$src][$dst]["QA"] = 0;
                    $dados[$src][$dst]["QN"] = 0;
                    $dados[$src][$dst]["QT"] = 0;
                    $dados[$src][$dst]["TA"] = 0;
                    $dados[$src][$dst]["TN"] = 0;
                    $dados[$src][$dst]["TT"] = 0;
                    $totais_q[$src] = 0;
                    $totais_t[$src] = 0;
                }
                switch ($val['disposition']) {
                    case "ANSWERED":
                        $dados[$src][$dst]["QA"]++;
                        $dados[$src][$dst]["TA"] += $val['duration'];
                        break;
                    default:
                        $dados[$src][$dst]["QN"]++;
                        $dados[$src][$dst]["TN"] += $val['duration'];
                        break;
                } // Fim do switch
                $dados[$src][$dst]["QT"]++;
                $dados[$src][$dst]["TT"] += $val['duration'];
                $totais_q[$src]++;
                $totais_t[$src] += $val['duration'];
            }
            unset($result);
            $result[$row['uniqueid']] = $row;
            $userfield = $row['userfield'];
        } // Fim do Foreach que varre o SELECT do CDR
        /* Switch a seguir é para pegar um possível último registro               */
        foreach ($result as $val) {
            $src = $val['src']; 
            $dst = $val['dst'];
            if (!isset($dados[$src])) {
                $dados[$src][$dst]["QA"] = 0;
                $dados[$src][$dst]["QN"] = 0;
                $dados[$src][$dst]["QT"] = 0;
                $dados[$src][$dst]["TA"] = 0;
                $dados[$src][$dst]["TN"] = 0;
                $dados[$src][$dst]["TT"] = 0;
                $totais_q[$src] = 0;
                $totais_t[$src] = 0;
            }
            switch ($val['disposition']) {
                case "ANSWERED":
                    $dados[$src][$dst]["QA"]++;
                    $dados[$src][$dst]["TA"] += $val['duration'];
                    break;
                default:
                    $dados[$src][$dst]["QN"]++;
                    $dados[$src][$dst]["TN"] += $val['duration'];
                    break;
            } // Fim do switch
            $dados[$src][$dst]["QT"]++;
            $dados[$src][$dst]["TT"] += $val['duration'];
            $totais_q[$src]++;
            $totais_t[$src] += $val['duration'];
        }
    } catch (Exception $e) {
        display_error($LANG['error'] . $e->getMessage(), true);
        exit;
    }

     
    if (count($dados) <= 1) {
        display_error($LANG['msg_notdata'], true);
        exit;
    }
    arsort($totais_q);
    arsort($totais_t); 
    
    // Rankear conforme selecao
    //$dados = array();
    $tot_view = $rank_num - 1;
    if ($rank_type == "qtdade") {
        foreach ($totais_q as $src => $qtd) {
            $ctd = $viewtop;
            if (isset($dados[$src])) {
                foreach ($dados[$src] as $dst => $val) {
                    if ($ctd == 0)
                        break;
                    $ctd--;
                    $rank[$src] [$val['QT']] [$dst] = $val;
                }
            }
            // Numero de origens a exibir
            if ($tot_view == 0)
                break;
            $tot_view--;
        }
    } else {
        foreach ($totais_t as $src => $qtd) {
            $ctd = $viewtop;
            if (isset($dados[$src])) {
                foreach ($dados[$src] as $dst => $val) {
                    if ($ctd == 0)
                        break;
                    $ctd--;
                    $rank[$src] [$val['TT']] [$dst] = $val;
                }
            }
            // Numero de origens a exibir
            if ($tot_view == 0)
                break;
            $tot_view--;
        }
    }
        
  // ordenar por SRC + Quantidades
    foreach ($rank as $src => $vqtd) {
        krsort($vqtd);
        foreach ($vqtd as $qtd => $vdst) {
            foreach ($vdst as $dst => $val) {
                $rank_final[$src][$qtd][$dst] = $val;
            }
        }
    }
    // Removendo as nao rechamdas
    if ($viewrec == 'yes') {
        foreach ($rank_final as $chaves => $valores) {
            $TTG = 0; 
            foreach ($valores as $key => $value) {
                foreach ($value as $k => $v) {
                    $TTG += $v['QT'];
                }
            }
            if ($TTG === 1) {
                unset($rank_final[$chaves]);
            }
        }
    }



    $_SESSION['tit_date'] = $TIT_DATE;
    $_SESSION['totais_t'] = $totais_t;
    $_SESSION['totais_q'] = $totais_q;
    $_SESSION['rank_final'] = $rank_final;
    
    echo "<meta http-equiv='refresh'  content='0; url=./rel_ranking.php?acao=imp&t=$acao'>\n";
}

/**
 * exibe_relatorio - Exibe o relatório na tela
 */
function exibe_relatorio() {

    $totais_t = $_SESSION['totais_t'];
    $totais_q = $_SESSION['totais_q'];
    $rank_final = $_SESSION['rank_final'];
    $TIT_DATE = $_SESSION['tit_date'];
    $tp_rel = $_GET['t'];
    $rank_type = $_SESSION['relrank']['rank_type'];


    global $db, $smarty, $SETUP, $LANG, $tipos_disp, $acao;

    $csv_rel_ranking = '';

    if ($tp_rel == "csv") {

        $rank_geral = array();

        foreach ($rank_final as $chaves => $valores) {

            $rank = array();

            foreach ($valores as $key => $value) {

                foreach ($value as $k => $v) {
                    $rank['origem'] = $chaves;
                    $rank['destino'] = $k;
                    $rank['QA'] = $v['QA'];
                    $rank['QN'] = $v['QN'];
                    $rank['QT'] = $v['QT'];
                    $rank['TA'] = $v['TA'];
                    $rank['TN'] = $v['TN'];
                    $rank['TT'] = $v['TT'];
                    $rank_geral[] = $rank;
                }
            }
        }

        $titulo = array(
            "origem" => 'Origem',
            "destino" => 'Destino',
            "QA" => 'qt_atendida',
            "QN" => 'qt_nao tendida',
            "QT" => 'qt_total',
            "TA" => 'tempo_atendidas',
            "TN" => 'tempo_nao atendidas',
            "TT" => 'total tempo'
        );
        $csv_rel_ranking = monta_csv($titulo, $rank_geral);
    }

// Cria Objeto para formatacao de dados
    $my_object = new Formata;
    $smarty->register_object("formata", $my_object);
    $smarty->assign('DADOS', $rank_final);
    ($rank_type == "qtdade" ? $smarty->assign('TOTAIS', $totais_q) : $smarty->assign('TOTAIS', $totais_t) );
    $smarty->assign('ARQCVS', $csv_rel_ranking);
    $smarty->assign('TPREL', $tp_rel);
    $smarty->assign('RANKTYPE', $rank_type);
    $titulo = $LANG['menu_reports'] . " » " . $LANG['menu_callranking'] . "<br />";
    $titulo.= $TIT_DATE;
    display_template("rel_ranking_view.tpl", $smarty, $titulo);
    exit;
}

/**
 * verificaCC - Verifica se existe módulo SnepCC e tabela cc_configuration
 * @return <String> Retorna valor da visualização ou false
 */
function verificaCC() {

    global $db;

    if (class_exists("Cc_AgentsInfo") || class_exists("Cc_Statistical")) {

        $db = Zend_Registry::get('db');
        $select = "SHOW TABLES LIKE 'cc_configuration'";
        $stmt = $db->query($select);
        $data = $stmt->fetch();

        if ($data != false) {

            $select = "Select preview from cc_configuration";

            $stmt = $db->query($select);
            $preview = $stmt->fetch();
            $result = $preview['preview'];

            return $result;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

/**
 * ListaAgentes - Verifica se a origem ou destino é agente
 * @return <array> Array de agentes
 */
function ListaAgentes() {

    $agentFile = '/etc/asterisk/snep/snep-agents.conf';
    $agents = explode("\n", file_get_contents($agentFile));
    $agentsData = array();

    foreach ($agents as $agent) {
        if (preg_match('/^agent/', $agent)) {
            $info = explode(",", substr($agent, 9));
            $agentsData[] = array('code' => $info[0], 'password' => $info[1], 'name' => $info[2]);
        }
    }
    return $agentsData;
}