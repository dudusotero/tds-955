<?php

/**
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Classe ccustos implementa as funcionalidades do centro de custo.
 *
 * @category  Snep
 * @package   src_ccustos
 * @copyright Copyright (c) 2010 OpenS Tecnologia
 * @author Opens Tecnologia
 */
require_once("../includes/verifica.php");
require_once("../configs/config.php");

ver_permissao(13);

// Variaveis de ambiente do form
$smarty->assign('ACAO', $acao);
$smarty->assign('TIPOS_CCUSTOS', $tipos_ccustos);

if ($acao == "cadastrar") {
    cadastrar();
} elseif ($acao == "alterar") {
    $titulo = $LANG['menu_register'] . " » " . $LANG['menu_ccustos'] . " » " . $LANG['change'];
    alterar();
} elseif ($acao == "grava_alterar") {
    grava_alterar();
} elseif ($acao == "excluir") {
    excluir();
} else {
    $titulo = $LANG['menu_register'] . " » " . $LANG['menu_ccustos'] . " » " . $LANG['include'];
    principal();
}

/**
 * principal - Monta a tela principal da rotina.
 */
function principal() {
    global $smarty, $titulo, $LANG, $db;
    $smarty->assign('dt_ccustos', array("tipo" => "E"));
    $smarty->assign('ACAO', "cadastrar");
    $smarty->assign('PROTOTYPE', true);
    display_template("ccustos.tpl", $smarty, $titulo);
}

/**
 * cadastrar - Inclui novo centro de custo no sistema.
 */
function cadastrar() {
    global $LANG, $db, $nome, $tipo, $descricao, $codigo;

    $select = $db->select()
            ->from('ccustos', array('codigo'))
            ->where("codigo = ?", $codigo);
    $stmt = $db->query($select);
    $result = $stmt->fetchAll();
    if (count($result) > 0) {
        display_error($LANG['error'] . 'Centro de custos já existe', true);
    }

    $nome = addslashes($nome);
    $sql = "INSERT INTO ccustos ";
    $sql .= " (codigo, nome, tipo, descricao)";
    $sql .= " VALUES ('$codigo','$nome','$tipo','$descricao')";
    try {
        $db->beginTransaction();
        $db->exec($sql);
        $db->commit();
        
        $tabela = "";
        $tabela = verificaLog($tabela);
        if ($tabela == true) {
            $id = $codigo;
            $acao = "Adicionou Centro de Custos";
            salvaLog($acao, $id);
            $action = "ADD";
            $add = getCcustos($id);
            
            insertLogCcustos($action, $add);
        }

        echo "<meta http-equiv='refresh' content='0;url=../index.php/costcenter/' >\n";
    } catch (Exception $e) {
        $db->rollBack();
        display_error($LANG['error'] . $e->getMessage(), true);
    }
}

/**
 * alterar - Edita o centro de custo no sistema.
 */
function alterar() {
    global $LANG, $db, $smarty, $titulo, $acao;
    $codigo = isset($_POST['codigo']) ? $_POST['codigo'] : $_GET['codigo'];
    if (!$codigo) {
        display_error($LANG['msg_notselect'], true);
        exit;
    }
    try {
        $cod_father = $cod_sun = "";
        // Pega 1a. PArte do Centro de Custo
        if (strlen($codigo) > 1) {
            $sql = "SELECT * FROM ccustos WHERE codigo='" . substr($codigo, 0, 1) . "'";
            $row = $db->query($sql)->fetch();
            $cod_father = $row['codigo'] . " - " . $row['nome'];
        }
        if (strlen($codigo) > 4) {
            $sql = "SELECT * FROM ccustos WHERE codigo='" . substr($codigo, 0, 4) . "'";
            $row = $db->query($sql)->fetch();
            $cod_sun = $row['codigo'] . " - " . $row['nome'];
        }
        $sql = "SELECT * FROM ccustos WHERE codigo='$codigo'";
        $row = $db->query($sql)->fetch();
    } catch (PDOException $e) {
        display_error($LANG['error'] . $e->getMessage(), true);
    }
    $smarty->assign('ACAO', "grava_alterar");
    $smarty->assign('dt_ccustos', $row);
    $smarty->assign('family', array("father" => $cod_father, "sun" => $cod_sun));
    display_template("ccustos.tpl", $smarty, $titulo);
}

/**
 * grava_alterar - Grava registro alterado.
 */
function grava_alterar() {
    global $LANG, $db, $codigo, $nome, $tipo, $descricao;
    
        $tabela = "";
        $tabela = verificaLog($tabela);
        if ($tabela == true) {
            $action = "OLD";
            $add = getCcustos($codigo);
            insertLogCcustos($action, $add);
        }
    
    $sql = "update ccustos set nome='$nome', tipo='$tipo', descricao='$descricao'";
    $sql .= "  where codigo='$codigo'";
    try {
        $db->beginTransaction();
        $db->exec($sql);
        $db->commit();
        
        if ($tabela == true) {
            $acao = "Editou Centro de Custos";
            salvaLog($acao, $codigo);
            $action = "NEW";
            $add = getCcustos($codigo);
            insertLogCcustos($action, $add);
        }
        
        echo "<meta http-equiv='refresh' content='0;url=../index.php/costcenter/'>\n";
    } catch (Exception $e) {
        $db->rollBack();
        display - error($LANG['error'] . $e->getMessage(), true);
    }
}

/**
 * excluir - Exclui registro selecionado.
 */
function excluir() {
    global $LANG, $db;
    $codigo = $_GET['codigo'];
    if (!$codigo) {
        display_error($LANG['msg_notselect'], true);
        exit;
    }
    try {
        //validacao de regras de negocio, conscistencia de dados.
        $rules_query = "SELECT value FROM regras_negocio_actions_config as rconf WHERE rconf.key = 'ccustos' and value = '$codigo'";
        $regras = $db->query($rules_query)->fetchAll();
        if (count($regras) > 0) {
            $msg = "Centro de custos está sendo usado em alguma regras de negócio<br />\n";
            display_error($msg, true);
            exit(1);
        }
        
        $tabela = "";
        $tabela = verificaLog($tabela);
        if ($tabela == true) {
            $id = $codigo;
            $acao = "Excluiu Centro de Custos";
            salvaLog($acao, $id);
            $action = "DEL";
            $add = getCcustos($id);
            insertLogCcustos($action, $add);
        } 
        
        $sql = "DELETE FROM ccustos WHERE codigo='" . $codigo . "'";
        $db->beginTransaction();
        $db->exec($sql);
        $db->commit();
        
                
        echo "<meta http-equiv='refresh' content='0;url=../index.php/costcenter/'>\n";
    } catch (PDOException $e) {
        display_error($LANG['error'] . $e->getMessage(), true);
    }
}

/**
 * verificaLog - Verifica se existe módulo Loguser.
 * @param <boolean> $tabela
 * @return <boolean> True ou false
 */
function verificaLog($tabela) {
    if (class_exists("Loguser_Manager")) {
        $tabela = true;
    } else {
        $tabela = false;
    }
    return $tabela;
}

/**
 * salvalog - Insere dados da ação na tabela logs.
 * @param <String> $ação Ação feita pelo usuário
 * @param <String> $cc id do centro de custo
 * @return <boolean> True ou false
 */
function salvaLog($acao, $cc) {

    $db = Zend_Registry::get("db");
    $ip = $_SERVER['REMOTE_ADDR'];
    $hora = date('Y-m-d H:i:s');
    $tipo = 6;
    global $id_user;

    $acao = mysql_escape_string($acao);

    $sql = "INSERT INTO `logs` VALUES (NULL, '" . $hora . "', '" . $ip . "', '" . $id_user . "', '" . $acao . "', '" . $cc . "', '" . $tipo . "' , '" . NULL . "', '" . NULL . "', '" . NULL . "', '" . NULL . "')";

    if ($db->query($sql)) {
        return true;
    } else {
        return false;
    }
}

/**
 * getCcustos - Monta array com todos dados do centro de custo
 * @param <int> $id - Nome da fila
 * @return <array> $custo - Dados da fila
 */
function getCcustos($id) {

    $custo = array();

    $db = Zend_Registry::get("db");
    $sql = "SELECT * from  ccustos where codigo='$id'";
    
    $stmt = $db->query($sql);
    $custo = $stmt->fetch();
    
    return $custo;
}

/**
 * insertLogFila - insere na tabela logs_users os dados do centro de custo
 * @global <int> $id_user
 * @param <array> $add
 */
function insertLogCcustos($acao, $add) {

    $db = Zend_Registry::get("db");
    $ip = $_SERVER['REMOTE_ADDR'];
    $hora = date('Y-m-d H:i:s');

    $auth = Zend_Auth::getInstance();
    global $id_user;
    
    $select = "SELECT name from peers where id = '$id_user'";
    $stmt = $db->query($select);
    $id = $stmt->fetch();

    $sql = "INSERT INTO `logs_users` VALUES (NULL, '" . $hora . "', '" . $ip . "', '" . $id["name"] . "', '" . $add["codigo"] . "', '" . $add["tipo"] . "', '" . $add["nome"] . "', '" . $add["descricao"] . "', '" . "CCUSTOS" . "', '" . $acao . "')";
    $db->query($sql);
}

?>