<?php

/**
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Classe queues_group Implementa as funcionalidades do grupo de filas.
 *
 * @category  Snep
 * @package   src_queues_group
 * @copyright Copyright (c) 2014 OpenS Tecnologia
 * @author Opens Tecnologia
 */
require_once("../includes/verifica.php");
require_once("../configs/config.php");

ver_permissao(89);

// Variaveis de ambiente do form
$smarty->assign('ACAO', $acao);

if ($acao == "cadastrar") {
    cadastrar();
} elseif ($acao == "alterar") {
    $titulo = $LANG['menu_register'] . " » " . $LANG['menu_grupos_queues'] . " » " . $LANG['change'];
    alterar();
} elseif ($acao == "grava_alterar") {
    grava_alterar();
} elseif ($acao == "excluir") {
    excluir();
} else {
    $titulo = $LANG['menu_register'] . " » " . $LANG['menu_grupos_queues'] . " » " . $LANG['include'];
    principal();
}

/**
 * principal - Monta a tela principal da rotina.
 */
function principal() {
    global $smarty, $titulo, $LANG, $db;

    try {
        
        $sql = "SELECT name FROM queues";

        $result = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        display_error($LANG['error'] . $e->getMessage(), true);
    }


    $queues = array();
    
    foreach ($result as $key => $val) {
        $queues[$val['name']] = $val['name'];
    }

    $smarty->assign('CONTACTS', $queues);
    $smarty->assign('ACAO', "cadastrar");

    display_template("queues_group.tpl", $smarty, $titulo);
}

/**
 * cadastrar - Inclui um novo grupo de filas.
 */
function cadastrar() {
    global $LANG, $db, $nome;
    
    $sql = "INSERT INTO group_queues (name) ";
    $sql .= "VALUES ('$nome')";
    $db->beginTransaction();
    $db->exec($sql);

    $group_id = $db->lastInsertId();

    $filas = ( isset($_POST['lista2']) ? $_POST['lista2'] : null );

    if ($filas) {
        foreach ($filas as $id) {
            $sql = "INSERT INTO members_group_queues (name_queue, id_group) VALUES ('$id',$group_id)";
            $db->exec($sql);
        }
    }

    try {
        $db->commit();
    } catch (Exception $e) {
        $db->rollBack();
        display_error($LANG['error'] . $e->getMessage(), true);
    }

    echo "<meta http-equiv='refresh' content='0;url=../index.php/queuesgroups'>\n";
}

/**
 * alterar - Alterar um grupo de fila.
 */
function alterar() {

    global $LANG, $db, $smarty, $titulo, $acao;

    $id = isset($_GET['cod_grupo']) ? $_GET['cod_grupo'] : null;

    if ($id === null) {
        display_error($LANG['msg_notselect'], true);
        exit;
    }

    try {
        $sql = "SELECT * FROM group_queues WHERE id='$id'";
        $row = $db->query($sql)->fetch();

    } catch (PDOException $e) {
        display_error($LANG['error'] . $e->getMessage(), true);
    }

    // Group members
    $sql = "SELECT * from members_group_queues WHERE id_group = $id";
    $members = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    
    // Queues
    $sql = "SELECT name FROM queues";
    $queuesAll = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    foreach($queuesAll as $key => $queue){
        foreach($members as $cha => $member){

            if($queue['name'] == $member["name_queue"]){
                $queuesAll[$key]['belongs'] = true;
            }
        }
    }

    $all_contacts = array();
    $pertence = array();

    foreach ($queuesAll as $key => $val) {
        
        if ($val['belongs']) {
            $pertence[$val['name']] = $val['name'];
        } else {
            $all_contacts[$val['name']] = $val['name'];
        }
    }
    
    $smarty->assign('PERTENCE', $pertence);
    $smarty->assign('CONTACTS', $all_contacts);
    $smarty->assign('EDITAR', 1);
    $smarty->assign('ACAO', "grava_alterar");

    $smarty->assign('dt_grupos', $row);
    display_template("queues_group.tpl", $smarty, $titulo);
}

/**
 * grava_alterar - Grava registro alterado
 */
function grava_alterar() {
    global $LANG, $db, $nome, $type, $lista1, $lista2;

    $id = isset($_GET['cod_grupo']) ? $_GET['cod_grupo'] : null;
    
    if ($id === null) {
        display_error($LANG['msg_notselect'], true);
        exit;
    }

    $sql = "UPDATE group_queues SET name='$nome' where id='$id'";
    $db->beginTransaction();
    $db->exec($sql);

    $sql = "DELETE FROM members_group_queues WHERE id_group='$id'";
    $db->exec($sql);
    
    foreach ($lista2 as $name_queue) {
        
        $sql = "INSERT INTO members_group_queues (name_queue, id_group) VALUES ('$name_queue',$id)";
        $db->exec($sql);
    }

    try {
        $db->commit();
    } catch (Exception $e) {
        $db->rollBack();
        display_error($LANG['error'] . $e->getMessage(), true);
    }
    echo "<meta http-equiv='refresh' content='0;url=../index.php/queuesgroups'>\n";
}

/**
 * excluir - Exclui registro selecionado
 */
function excluir() {
    global $LANG, $db, $smarty, $titulo, $acao;

    $id = isset($_GET['cod_grupo']) ? $_GET['cod_grupo'] : null;

    if ($id === null) {
        display_error($LANG['msg_notselect'], true);
        exit;
    }

    $sql = "DELETE FROM members_group_queues WHERE id_group='$id'";
    $db->exec($sql);

    $sql = "DELETE FROM group_queues WHERE id='$id'";
    $db->exec($sql);

    echo "<meta http-equiv='refresh' content='0;url=../index.php/queuesgroups'>\n";

}



