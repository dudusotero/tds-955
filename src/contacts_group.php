<?php

/**
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Classe contacts_group Implementa as funcionalidades do grupo de contatos.
 *
 * @category  Snep
 * @package   src_contacts_group
 * @copyright Copyright (c) 2010 OpenS Tecnologia
 * @author Opens Tecnologia
 */
require_once("../includes/verifica.php");
require_once("../configs/config.php");

ver_permissao(49);

// Variaveis de ambiente do form
$smarty->assign('ACAO', $acao);
if ($acao == "cadastrar") {
    cadastrar();
} elseif ($acao == "alterar") {
    $titulo = $LANG['menu_register'] . " » " . $LANG['contacts_group'] . " » " . $LANG['change'];
    alterar();
} elseif ($acao == "grava_alterar") {
    grava_alterar();
} elseif ($acao == "excluir") {
    excluir();
} elseif ($acao == "incluir") {
    incluir();
} elseif ($acao == "excluir_def") {
    excluir_def();
} elseif ($acao == "excluir_contatos") {
    excluir_contatos();
} else {
    $titulo = $LANG['menu_register'] . " » " . $LANG['contacts_group'] . " » " . $LANG['include'];
    principal();
}

/**
 * principal - Monta a tela principal da rotina.
 */
function principal() {
    global $smarty, $titulo, $LANG, $db;

    try {
        $sql = "SELECT c.id as id, c.name as name, g.name as `group` FROM contacts_names as c, contacts_group as g  WHERE (c.group = g.id) ";
        $contacts_result = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        display_error($LANG['error'] . $e->getMessage(), true);
    }
    $contacts = array();
    foreach ($contacts_result as $key => $val) {
        $contacts[$val['id']] = $val['name'] . " (" . $val['group'] . ")";
    }

    $smarty->assign('CONTACTS', $contacts);
    $smarty->assign('ACAO', "cadastrar");

    display_template("contacts_group.tpl", $smarty, $titulo);
}

/**
 * cadastrar - Inclui um novo grupo de contato.
 */
function cadastrar() {
    global $LANG, $db, $nome;
    $sql = "INSERT INTO contacts_group (name) ";
    $sql .= "VALUES ('$nome')";
    $db->beginTransaction();
    $db->exec($sql);

    $group_id = $db->lastInsertId();

    // Inclusão dos ramais selecionados no grupo recém criado.
    $contacts = ( isset($_POST['lista2']) ? $_POST['lista2'] : null );

    if ($contacts) {
        foreach ($contacts as $id) {
            $sql = "UPDATE contacts_names SET `group`='$group_id' WHERE id='$id' ";
            $db->exec($sql);
        }
    }

    try {
        $db->commit();
    } catch (Exception $e) {
        $db->rollBack();
        display_error($LANG['error'] . $e->getMessage(), true);
    }

    echo "<meta http-equiv='refresh' content='0;url=../index.php/contactsgroups'>\n";
}

/**
 * alterar - Alterar um grupo de contato.
 */
function alterar() {
    global $LANG, $db, $smarty, $titulo, $acao;
    $id = isset($_GET['cod_grupo']) ? $_GET['cod_grupo'] : null;
    if ($id === null) {
        display_error($LANG['msg_notselect'], true);
        exit;
    }

    try {
        $sql = "SELECT * FROM contacts_group WHERE id='$id'";
        $row = $db->query($sql)->fetch();
    } catch (PDOException $e) {
        display_error($LANG['error'] . $e->getMessage(), true);
    }

    /* Busca ramais pertencentes ao grupo */
    try {
        $sql = "SELECT c.id as id, c.name as name, g.name as `group`, g.id as group_id FROM contacts_names as c, contacts_group as g  WHERE (c.group = g.id) ";
        $contacts = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        display_error($LANG['error'] . $e->getMessage(), true);
    }

    $all_contacts = array();
    $pertence = array();
    foreach ($contacts as $key => $val) {
        if ($val['group'] == $row['name']) {
            $pertence[$val['id']] = $val['name'] . " (" . $val['group'] . ")";
        } else {
            $all_contacts[$val['id']] = $val['name'] . " (" . $val['group'] . ")";
        }
    }

    $smarty->assign('PERTENCE', $pertence);
    $smarty->assign('CONTACTS', $all_contacts);
    $smarty->assign('EDITAR', 1);
    $smarty->assign('ACAO', "grava_alterar");
    $smarty->assign('dt_grupos', $row);
    display_template("contacts_group.tpl", $smarty, $titulo);
}

/**
 * grava_alterar - Grava registro alterado
 */
function grava_alterar() {
    global $LANG, $db, $nome, $type, $lista1, $lista2;

    $id = isset($_GET['cod_grupo']) ? $_GET['cod_grupo'] : null;
    if ($id === null) {
        display_error($LANG['msg_notselect'], true);
        exit;
    }

    $sql = "UPDATE contacts_group SET name='$nome' where id='$id'";
    $db->beginTransaction();
    $db->exec($sql);

    foreach ($lista2 as $contact_id) {
        $sql_contacts = "UPDATE contacts_names SET `group`='$id' where id='$contact_id'";
        $db->exec($sql_contacts);
    }

    try {
        $db->commit();
    } catch (Exception $e) {
        $db->rollBack();
        display_error($LANG['error'] . $e->getMessage(), true);
    }
    echo "<meta http-equiv='refresh' content='0;url=../index.php/contactsgroups'>\n";
}

/**
 * excluir - Exclui registro selecionado
 */
function excluir() {
    global $LANG, $db, $smarty, $titulo, $acao;

    $id = isset($_GET['cod_grupo']) ? $_GET['cod_grupo'] : null;
    if ($id === null) {
        display_error($LANG['msg_notselect'], true);
        exit;
    }

    // Fazendo procura por referencia a esse grupo em regras de negócio.
    $rules_query = "SELECT id, `desc` FROM regras_negocio WHERE origem LIKE '%CG:$id%' OR destino LIKE '%CG:$id%'";
    $regras = $db->query($rules_query)->fetchAll();
    if (count($regras) > 0) {
        $msg = $LANG['group_conflict_in_rules'] . ":<br />\n";
        foreach ($regras as $regra) {
            $msg .= $regra['id'] . " - " . $regra['desc'] . "<br />\n";
        }
        display_error($msg, true);
    }

    $sql = "SELECT id, name FROM contacts_group WHERE id != '$id'";
    $row = $db->query($sql)->fetchAll();

    $grupos = array();
    foreach ($row as $key => $group) {
        $grupos[$group['id']] = $group['name'];
    }

    $smarty->assign('back_button', '../index.php/contactsgroups');
    $smarty->assign('ACAO', "grava_alterar");
    $smarty->assign('name', $id);
    $smarty->assign('dt_grupos', $grupos);
    display_template("groups_delete.tpl", $smarty, $titulo);
}

/**
 * excluir_contatos - Exclui contatos que estão no grupo.
 */
function excluir_contatos() {

    global $db, $LANG;
    $codigo = isset($_POST['cod_grupo']) ? $_POST['cod_grupo'] : $_GET['cod_grupo'];
    if (!$codigo) {
        display_error($LANG['msg_notselect'], true);
        exit;
    }

    $db->beginTransaction();

    $sql = "DELETE FROM contacts_names WHERE contacts_names.group='" . $codigo . "'";
    $db->exec($sql);

    try {
        $db->commit();
        echo "<meta http-equiv='refresh' content='0;url=../index.php/contactsgroups'>\n";
    } catch (PDOException $e) {
        $db->rollBack();
        display_error($LANG['error'] . $e->getMessage(), true);
    }
}

/**
 * excluir_def - Exclui definitivamente o registro selecionado
 */
function excluir_def() {
    global $LANG, $db;
    $codigo = isset($_POST['cod_grupo']) ? $_POST['cod_grupo'] : $_GET['cod_grupo'];
    if (!$codigo) {
        display_error($LANG['msg_notselect'], true);
        exit;
    }
    $new_group = isset($_POST['group']) ? $_POST['group'] : 'users';
    $db->beginTransaction();

    $sql = "UPDATE contacts_names SET `group`='$new_group' WHERE `group`='$codigo'";
    $db->exec($sql);

    $sql = "DELETE FROM contacts_group WHERE id='" . $codigo . "'";
    $db->exec($sql);

    try {
        $db->commit();
        echo "<meta http-equiv='refresh' content='0;url=../index.php/contactsgroups'>\n";
    } catch (PDOException $e) {
        $db->rollBack();
        display_error($LANG['error'] . $e->getMessage(), true);
    }
}
