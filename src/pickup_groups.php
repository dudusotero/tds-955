<?php
/**
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Classe pickup_groups Implementa as funcionalidades dos grupos de captura
 *
 * @category  Snep
 * @package   src_pickup_groups
 * @copyright Copyright (c) 2010 OpenS Tecnologia
 * @author Opens Tecnologia
 */

 require_once("../includes/verifica.php");  
 require_once("../configs/config.php");
 ver_permissao(12) ;
 
 // Variaveis de ambiente do form
 $smarty->assign('ACAO',$acao) ;
 
 if ($acao == "cadastrar") {
    cadastrar();
 } elseif ($acao ==  "alterar") {
    $titulo = $LANG['menu_register']." » ".$LANG['menu_grupos']." » ".$LANG['change'];
    alterar() ;
 } elseif ($acao ==  "grava_alterar") {
    grava_alterar() ;
 } elseif ($acao ==  "excluir") {
    excluir() ; 
 } elseif ($acao == "incluir") {
    incluir();
 } elseif ($acao == "incluir_ao_grupo") {
     incluir_ao_grupo();
 } else {
   $titulo = $LANG['menu_register']." » ".$LANG['menu_grupos']." » ".$LANG['include'];
   principal() ;
 }

 /**
 * principal - Monta a tela principal da rotina.
 */
 function principal()  {
   global $smarty, $titulo, $LANG, $db ;

   try{
        $sql = "select id, name, pickupgroup, nome, cod_grupo from peers left join grupos on cod_grupo = pickupgroup ";
        $sql.= " where name != 'admin' AND peer_type = 'R'";
        $ramais = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }catch(Exception $e) {
        display_error($LANG['error'].$e->getMessage(),true) ;
    }

    $ram = array();
    foreach($ramais as $key => $val) {
        $ram[$val['name']] = $val['name'] ;
        if ($val['cod_grupo'] != "") {
            $ram[$val['name']] = $val['name']." (".$val['cod_grupo']."-".$val['nome'] .")";
        }
    }
    $smarty->assign('RAMAIS', $ram);
    $smarty->assign('ACAO',"cadastrar"); 
    display_template("pickup_groups.tpl",$smarty,$titulo) ;
}

/**
 * cadastrar - Cadastra um novo grupo de captura no sistema
 */
function cadastrar()  {
   global $LANG, $db, $nome;
   $sql  = "INSERT INTO grupos (nome) " ;
   $sql .= "VALUES ('$nome')" ;
   try {
      $db->beginTransaction() ;
      $db->exec($sql) ;
      $group_id = $db->lastInsertId();

      // Inclusão dos ramais selecionados no grupo recém criado.
      $ramais = ( isset( $_POST['lista2'] ) ? $_POST['lista2'] : null ) ;

      if( $ramais ){
         foreach($ramais as $id => $val) {
            $sql  = " UPDATE peers SET `pickupgroup`='$group_id' WHERE name='$val' ";      
            $stmt = $db->prepare($sql);
            $stmt->execute() ;
         }
      }
      $db->commit();
   } catch (Exception $e) {
      $db->rollBack();
      display_error($LANG['error'].$e->getMessage(),true) ;
   }		
   echo "<meta http-equiv='refresh' content='0;url=../index.php/pickupgroups'>\n";
}

/**
 * alterar - Altera o registro selecionado
 */
function alterar()  {
   global $LANG,$db,$smarty,$titulo, $acao ;
   $codigo = isset($_POST['cod_grupo']) ? $_POST['cod_grupo'] : $_GET['cod_grupo'];
   if (!$codigo) {
      display_error($LANG['msg_notselect'],true) ;
      exit ;
   }
   try {
      $sql = "SELECT * FROM grupos WHERE cod_grupo=".$codigo;
      $row = $db->query($sql)->fetch();
   } catch (PDOException $e) {
      display_error($LANG['error'].$e->getMessage(),true) ;
   }
   /* Busca ramais pertencentes ao grupo */
   try{
      $sql = "select id, name, pickupgroup, nome, cod_grupo from peers left join grupos on cod_grupo = pickupgroup ";
      $sql.= " where name != 'admin' AND peer_type = 'R'";
      $ramais = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }catch(Exception $e) {
      display_error($LANG['error'].$e->getMessage(),true) ;
    }
    $ram = array();
    $pertence = array();
    foreach($ramais as $key => $val) {
        if($val['pickupgroup'] == $codigo ) {
            $pertence[$val['name']] = $val['name'] ." (".$val['cod_grupo']."-".$val['nome'] .")";
        }else{
            $ram[$val['name']] = $val['name'];
                if ($val['cod_grupo'] != "") {
                    $ram[$val['name']] = $val['name']." (".$val['cod_grupo']."-".$val['nome'] .")";
                }
        }
       
    }

    $smarty->assign('PERTENCE', $pertence);
    $smarty->assign('RAMAIS', $ram);
    $smarty->assign('EDITAR', 1);
    $smarty->assign('ACAO',"grava_alterar") ;
    $smarty->assign ('dt_grupos',$row);
    display_template("pickup_groups.tpl",$smarty,$titulo);
}

/**
 * grava_alterar - Grava registro alterado
 */
function grava_alterar()  {
    global $LANG, $db, $cod_grupo, $nome, $type, $lista2;

    try {
        $sql = "UPDATE grupos SET nome='$nome'  where cod_grupo='$cod_grupo'";
        $db->beginTransaction();
        $db->exec($sql);

        $sql_reset = "UPDATE peers SET pickupgroup=NULL where pickupgroup='{$cod_grupo}' ";
        $db->exec($sql_reset);
        
        foreach ($lista2 as $id => $wal) {
           $sql_peers = "UPDATE peers SET pickupgroup='$cod_grupo' where name='$wal' ";
           $db->exec($sql_peers);
        }    
        $db->commit();
    } catch (PDOException $e) {
        display_error($LANG['error'].$e->getMessage(),true) ;
    }
    echo "<meta http-equiv='refresh' content='0;url=../index.php/pickupgroups/'>\n" ;
}

/**
 * excluir - Exclui registro selecionado
 */
function excluir()  {
   global $LANG, $db;
   $codigo = isset($_POST['cod_grupo']) ? $_POST['cod_grupo'] : $_GET['cod_grupo'];
   if (!$codigo) {
      display_error($LANG['msg_notselect'],true) ;
      exit ;
   }
   try {
      $sql = "DELETE FROM grupos WHERE cod_grupo='".$codigo."'";
      $db->beginTransaction() ;
      $db->exec($sql) ;
      $db->commit();
      echo "<meta http-equiv='refresh' content='0;url=../index.php/pickupgroups/'>\n" ;
 } catch (PDOException $e) {
    display_error($LANG['error'].$e->getMessage(),true) ;
 }
}
