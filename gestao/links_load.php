<?php
/**
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Classe que monta a tela de links Khomp
 *
 * @category  Snep
 * @package   gestao_links_load
 * @copyright Copyright (c) 2010 OpenS Tecnologia
 * @author    Opens Tecnologia
 */
require_once("../includes/verifica.php");
require_once("../configs/config.php");
ver_permissao(35);



if (!$data = ast_status("khomp links show concise", "", True)) {
    display_error($LANG['msg_nosocket'], true);
    exit;
}

if ($acao == "relatorio") {
    exibe_monitor();
    exit;
}

$lines = explode("\n", $data);
$links = array();
$boards = array();
$lst = '';

if (trim(substr($lines['1'], 10, 16)) === "Error" || strpos($lines['1'], "such command") > 0) {
    display_error($LANG['msg_nokhomp'], true);
}

while (list($key, $val) = each($lines)) {
    if (substr($val, 0, 1) === "B" && substr($val, 3, 1) === "L") {
        if (substr($val, 0, 3) != $lst) {
            $board = substr($val, 0, 3);
            $boards[] = $board;
            $lnk = substr($val, 3, 3);
            $status = trim(substr($val, strpos($val, ":") + 1));
            $links[$board][$lnk] = $khomp_signal[$status];
            $lst = $board;
        }
    }
}
$titulo = $LANG['menu_status']." » ".$LANG['menu_links'];
$smarty->assign('PLACAS', $boards);
display_template("links_load.tpl", $smarty, $titulo);

/**
 * exibe_monitor - Visualização das informações dos links khomp
 * @global type $smarty
 * @global type $SETUP
 */
function exibe_monitor() {

    global $smarty, $SETUP;

    $_SESSION['tiporel'] = $_POST['tiporel'];
    $_SESSION['statusk'] = $_POST['statusk'];
    
    $listplacas = $_POST['listplacas'];
    $smarty->assign('REFRESH', array('mostrar' => True,
        'tempo' => $SETUP['ambiente']['tempo_refresh'],
        'url' => "../gestao/links.php?placas=" . implode(';', $listplacas)));
    $titulo = "Status » Links Khomp";;
    $smarty->assign ('LINKS_KHOMP', $links_khomp_lista);
    display_template("cabecalho.tpl", $smarty, $titulo);
}
