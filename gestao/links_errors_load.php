<?php
/**
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Classe links_errors_load implementa os erros de links Khomp
 *
 * @category  Snep
 * @package   gestao_links_errors_load
 * @copyright Copyright (c) 2010 OpenS Tecnologia
 * @author Opens Tecnologia
 */
require_once("../includes/verifica.php");
require_once("../configs/config.php");
ver_permissao(105);

if (!$data = ast_status("khomp links show concise","",True )) {
   display_error($LANG['msg_nosocket'], true) ;
   exit;
}

$lines = explode("\n", $data);


if (trim(substr($lines['1'], 10, 16)) === "Error" || strpos($lines['1'], "such command") > 0) {
    display_error($LANG['msg_nokhomp'], true);
}

$titulo = "Status » Erros Links Khomp" ;
$smarty->assign ('REFRESH',array('mostrar'=> true,
                              'tempo'  => $SETUP['ambiente']['tempo_refresh'],
                              'url'    => "../gestao/links_errors.php"));
display_template("cabecalho.tpl",$smarty,$titulo) ;
