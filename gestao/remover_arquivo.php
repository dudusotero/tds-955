<?php

/**
 *  This file is part of SNEP.
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 * ----------------------------------------------------------------------------
 * Programa: manutencao.js - Conjunto de funcoes para manutencao do sistema.
 * Copyright (c) 2007 - Opens Tecnologia - Projeto SNEP
 * Licenciado sob Creative Commons. Veja arquivo ./doc/licenca.txt
 * Autor: Rafael Bozzetti <rafael@opens.com.br>
 * ---------------------------------------------------------------------------- */
require_once("../includes/verifica.php");
require_once("../configs/config.php");
$arquivo = $_GET['arquivo'];
$arquivo = "/var/www" . $arquivo;
if (file_exists($arquivo)) {
    unlink($arquivo);
}
echo "<script type='text/javascript'> history.go(-1) </script>";
?>

