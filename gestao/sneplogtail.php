<?php
/**
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Classe sneplogtail implementa os logs do sistema conforme número de linhas escolhidos
 *
 * @category  Snep
 * @package   default_ContactsController
 * @copyright Copyright (c) 2010 OpenS Tecnologia
 * @author Opens Tecnologia
 */

require_once("../includes/verifica.php");
require_once("../configs/config.php");


$n = ( isset($_POST['n']) ? $_POST['n'] : 30 );

$log = new Snep_Log($smarty->agi_log, 'agi.log');

if($log != 'error') {
   $res = $log->getTail($n);
}else{
   echo "Arquivo de log não encontrado";   
   exit ;
}

print_r($res);