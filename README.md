#O que é o SNEP Livre

O SNEP é um software PBX baseado em Asterisk e GNU/Linux licenciado sob GPL v2 capaz de rodar em pequenos hardwares com uma boa performance. Possui diversos recursos de administração que darão flexibilidade, agilidade e produtividade na comunicação de voz das empresas. Pode ser customizado de acordo com a necessidade de cada negócio. Possui todas as funcionalidades de uma central telefônica de grande porte: Voice mail, gravação, roteamento avançado de ligações, cadeado, sem limites de ramais e muito mais.

##Funcionalidades da Interface
 
* Administração de ramais e grupos
* Administração de filas
* Administração de troncos: SIP, IAX, TDM, GSM
* Salas de conferência
* Logs do sistema
* Administração de arquivos de som e músicas de espera
* Configurações gerais do sistema
* Configurações simplificadas de rotas (baseado em múltiplas origens, destinos e horários)
* Gravação de chamadas sob demanda ou regras de gravação
* Administração de rotas de entrada
* Gestão das chamadas por centro de custos
* Simulador de rotas
* Escuta de gravações
	
##Controle por Centros de Custos

* Distribuição inteligente de chamadas, realocação de recursos em tempo real, registro e tarifação de chamadas e muito mais!
	
##Automatização de Procedimentos

* Automatização de procedimentos de fax, distribuição e priorização de chamadas, autogestão de filas de atendimento, rotas dinâmicas de saída e muito mais!
	
##Estabilidade

* Monitoramento do sistema, alerta de eventos, status do sistema, redundância de hardware e software, autonomia elétrica e muito mais!
	
##Excelência de Atendimento

* Monitoramento de SLA, de atendimentos, flexibilidade de distribuição e muito mais!