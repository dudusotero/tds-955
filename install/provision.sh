#!/bin/bash

eval `ssh-agent`

download()
{
    echo "Remove SNEP Old"
    rm -rf /var/www/sneplivre

    echo "Download snep"
    git clone git@bitbucket.org:snepdev/snep-1.3.git /home/vagrant/sneplivre
    cd /home/vagrant/sneplivre && git checkout $1 && git pull
}

moving_files()
{
    echo "Moving files"
    mv /home/vagrant/sneplivre /var/www/sneplivre
}

reset_permissions()
{
    echo "Reset permissions"
    chown www-data.www-data /var/www/sneplivre -R
}

finish()
{
    reset_permissions
    echo "Finishing snep provision at $(date)"
    date > /tmp/provision_sneplivre_at
}

run()
{
    echo "Starting snep branch $1 provision at $(date)"
    download $1
    moving_files
    finish
}

run $1
