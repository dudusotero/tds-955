CREATE INDEX lista_abandono_data ON lista_abandono (data);
CREATE INDEX lista_abandono_fila ON lista_abandono (fila(150));
CREATE INDEX lista_abandono_evento ON lista_abandono (evento(150));
CREATE INDEX lista_abandono_date ON lista_abandono (date);
CREATE INDEX lista_abandono_canal ON lista_abandono (canal(150));
