ALTER TABLE regras_negocio ADD from_dialer tinyint(1) NOT NULL default 0;

CREATE TABLE IF NOT EXISTS `group_queues` (
  `id` integer NOT NULL auto_increment,
  `name` varchar(120) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

ALTER TABLE queues ADD idGroup integer NOT NULL;

UPDATE queues SET `idGroup` = 1;
ALTER TABLE queues ADD CONSTRAINT group_queues_fk FOREIGN KEY (`idGroup`) REFERENCES group_queues(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

INSERT INTO `rotinas` VALUES (89,'Grupo de Filas');


