ALTER TABLE  `alertas` ADD  `disparo` BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE `regras_negocio`  ADD `mailing` BOOLEAN NOT NULL;

--
-- Indexação da tabela `cdr`
--
CREATE INDEX cdr_clid ON cdr (clid(30));
CREATE INDEX cdr_src ON cdr (src(30));
CREATE INDEX cdr_dst ON cdr (dst(30));
CREATE INDEX cdr_dcontext ON cdr (dcontext(30));
CREATE INDEX cdr_channel ON cdr (channel(30));
CREATE INDEX cdr_dstchannel ON cdr (dstchannel(50));
CREATE INDEX cdr_lastapp ON cdr (lastapp(30));
CREATE INDEX cdr_lastdata ON cdr (lastdata(50));
CREATE INDEX cdr_disposition ON cdr (disposition(30));
CREATE INDEX cdr_accountcode ON cdr (accountcode(20));
CREATE INDEX cdr_uniqueid ON cdr (uniqueid(32));
CREATE INDEX cdr_userfield ON cdr (userfield(120));

