--
-- Table structure for table `members_group_queues`
--
CREATE TABLE IF NOT EXISTS `members_group_queues` (
  `id` integer NOT NULL auto_increment,
  `name_queue` varchar(128) NOT NULL,
  `id_group` int NOT NULL,
  PRIMARY KEY  (`id`),
  FOREIGN KEY (name_queue) REFERENCES queues(name) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (id_group) REFERENCES group_queues(id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Ajustes tabela queues
--
ALTER TABLE `queues` DROP FOREIGN KEY group_queues_fk; 
ALTER TABLE `queues` DROP `idGroup`;

--
-- Ajustes tabela alertas
--
ALTER TABLE `alertas` DROP COLUMN `disparo` ;
ALTER TABLE `alertas` ADD COLUMN `ultimo_disparo` VARCHAR(32) NULL ;
ALTER TABLE `alertas` ADD COLUMN `email` VARCHAR(50) NULL ;

--
-- Ajustes na tabela ARS
--
INSERT INTO `ars_cidade` VALUES (15709,"MARAU");
INSERT INTO `ars_ddd` VALUES (54,"RS",15709);
UPDATE `ars_prefixo` SET cidade=15709 WHERE prefixo=543342;

