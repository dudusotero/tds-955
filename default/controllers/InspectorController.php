<?php
/**
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Classe que monta a tela de diagnóstico do sistema
 *
 * @category  Snep
 * @package   default_InspectorController
 * @copyright Copyright (c) 2010 OpenS Tecnologia
 * @author    Opens Tecnologia
 */

class InspectorController extends Zend_Controller_Action {
    
    /**
    * indexAction - Monta tela principal da rotina  
    */
    public function indexAction() {

        $this->view->breadcrumb = $this->view->translate("Configurações » Diagnóstico do Sistema");

        // cria objeto da classe Snep_Inspector
        $obj = new Snep_Inspector();

        // Pega array de informacoes gerados no construtor
        $inspect = $obj->getInspects();

        $this->view->inspect = $inspect;
    }
}


