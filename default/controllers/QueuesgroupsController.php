<?php
/**
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Classe QueuesgroupsController implementa a tela principal do grupo de filas
 *
 * @category  Snep
 * @package   default_QueuesgroupsController
 * @copyright Copyright (c) 2014 OpenS Tecnologia
 * @author Tiago Zimmermann <tiago.zimmermann@opens.com.br>
 */

class QueuesgroupsController extends Zend_Controller_Action {

	/**
	 * List queues groups
	 */
	public function indexAction() {

        $this->view->breadcrumb = $this->view->translate("Cadastro » Grupos de Filas");

        $db = Zend_Registry::get('db');
        $select = $db->select()
                        ->from("group_queues");
                        

        if ($this->_request->getPost('filtro')) {
            $field = mysql_escape_string($this->_request->getPost('campo'));
            $query = mysql_escape_string($this->_request->getPost('filtro'));
            $select->where("`$field` like '%$query%'");
        }

        $page = $this->_request->getParam('page');
        $this->view->page = ( isset($page) && is_numeric($page) ? $page : 1 );

        $paginatorAdapter = new Zend_Paginator_Adapter_DbSelect($select);
        $paginator = new Zend_Paginator($paginatorAdapter);

        $paginator->setCurrentPageNumber($this->view->page);
        $paginator->setItemCountPerPage(Zend_Registry::get('config')->ambiente->linelimit);
        
        $this->view->pathweb = Zend_Registry::get('config')->system->path->web;

        $this->view->groups = $paginator;
        $this->view->pages = $paginator->getPages();        
        $this->view->PAGE_URL = "{$this->getFrontController()->getBaseUrl()}/{$this->getRequest()->getModuleName()}/{$this->getRequest()->getControllerName()}/index/";
        $this->view->url = $this->getFrontController()->getBaseUrl() ."/". 
                            $this->getRequest()->getModuleName() ."/queuesgroups/";


        $options = array("name" => $this->view->translate("Nome") );

        $filter = new Snep_Form_Filter();
        $filter->setAction( $this->view->url . '/index/');
        $filter->setValue($this->_request->getPost('campo'));
        $filter->setFieldOptions($options);
        $filter->setFieldValue($this->_request->getParam('filtro'));
        $filter->setResetUrl("{$this->view->url}/index/page/$page");

        $this->view->form_filter = $filter;
        $this->view->filter = array( array("url" => "/snep/src/queues_group.php",
                                           "display" => $this->view->translate("Incluir Grupo de Filas"),
                                           "css" => "include"),
                                   );
    }

}

?>