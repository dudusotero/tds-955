<?php

require_once 'Zend/Controller/Action.php';

/**
 * Classe ErrorController
 *
 * @category  Snep
 * @package   default_ErrorController
 * @copyright Copyright (c) 2010 OpenS Tecnologia
 * @author Opens Tecnologia
 */
class ErrorController extends Zend_Controller_Action {
    
    /**
     * errorAction - Monta página de erro
     */
    public function errorAction() {
        $errors = $this->_getParam('error_handler');

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:

                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $this->view->message = 'A pagina a qual você está procurando não foi encontrada.';
                $this->view->title = '404 - Não Encontrado';
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $this->view->title = '500 - Erro Interno';
                $this->view->sidebar = false;
                $this->view->message = 'Por favor, contate o suporte o administrador do sistema.';
                break;
        }

        $this->view->exception = $errors->exception;
        $this->view->request   = $errors->request;
        $this->view->hideMenu  = true;
        $this->view->headTitle($this->view->title, 'PREPEND');
    }
}
