<?php
/**
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Classe IndexController implementa a tela principal do snep
 *
 * @category  Snep
 * @package   default_IndexController
 * @copyright Copyright (c) 2010 OpenS Tecnologia
 * @author Opens Tecnologia
 */

require_once 'Zend/Controller/Action.php';

class IndexController extends Zend_Controller_Action {
    
    /**
     * indexAction - redirecionar para página principal ou tela de instalação 
     */
    public function indexAction() {
        // Direcionando para o "snep antigo"
        $config = Zend_Registry::get('config');

        if( trim ( $config->ambiente->db->host ) == "" ) {            
            $this->_redirect("/installer/");

        }else{            
            $this->_redirect("../src/sistema.php");
        }
        
    }
}
