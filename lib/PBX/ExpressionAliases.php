<?php

/**
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/lgpl.txt>.
 */

/**
 * Faz o controle em banco dos Alias para expressões regulares.
 *
 * @category  Snep
 * @package   Snep
 * @copyright Copyright (c) 2010 OpenS Tecnologia
 * @author Henrique Grolli Bassotto
 */
class PBX_ExpressionAliases {

    private static $instance;

    protected function __construct() {
        
    }

    protected function __clone() {
        
    }

    /**
     * Retorna instancia dessa classe
     *
     * @return PBX_ExpressionAliases
     */
    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getAll() {
        $db = Zend_Registry::get('db');
        $select = "SELECT aliasid, name FROM expr_alias";

        $stmt = $db->query($select);
        $raw_aliases = $stmt->fetchAll();

        $aliases = array();
        foreach ($raw_aliases as $alias) {
            $aliases[$alias['aliasid']] = array(
                "id" => $alias['aliasid'],
                "name" => $alias['name'],
                "expressions" => array()
            );
        }

        $db = Zend_Registry::get('db');
        $select = "SELECT aliasid, expression FROM expr_alias_expression";

        $stmt = $db->query($select);
        $raw_expressions = $stmt->fetchAll();

        foreach ($raw_expressions as $expr) {
            $aliases[$expr["aliasid"]]["expressions"][] = $expr['expression'];
        }

        return $aliases;
    }

    public function get($id) {
        if (!is_integer($id)) {
            throw new PBX_Exception_BadArg("Id must be numerical");
        }

        $db = Zend_Registry::get('db');
        $select = "SELECT name FROM expr_alias WHERE aliasid='$id'";

        $stmt = $db->query($select);
        $raw_alias = $stmt->fetchObject();
        $alias = array(
            "id" => $id,
            "name" => $raw_alias->name,
            "expressions" => array()
        );

        $db = Zend_Registry::get('db');
        $select = "SELECT expression FROM expr_alias_expression WHERE aliasid='$id'";

        $stmt = $db->query($select);
        $raw_expression = $stmt->fetchAll();

        foreach ($raw_expression as $expr) {
            $alias["expressions"][] = $expr['expression'];
        }

        return $alias;
    }

    public function register($expression) {
        $db = Zend_Registry::get('db');

        $db->beginTransaction();
        $db->insert("expr_alias", array("name" => $expression['name']));
        $id = $db->lastInsertId();

        foreach ($expression['expressions'] as $expr) {
            $data = array("aliasid" => $id, "expression" => $expr);
            $db->insert("expr_alias_expression", $data);
        }

        $tabela = "";
        $tabela = self::verificaLog($tabela);
        if ($tabela == true) {
            //log
            $acao = "Adicionou expressao regular";
            self::salvaLog($acao, $id);
            $action = "ADD";
            
            $add["id"] = $id;
            $add["name"] = $expression["name"];
            $add["exp"] = "";
            foreach ($expression['expressions'] as $expr) {

                $add["exp"] .= $expr . "  ";
            }
            self::insertLogExpression($action, $add);
        }

        try {
            $db->commit();
        } catch (Exception $ex) {
            $db->rollBack();
            throw $ex;
        }
    }

    public function update($expression) {
        $id = $expression['id'];
                
        $tabela = "";
        $tabela = self::verificaLog($tabela);
        if ($tabela == true) {
                      
            $action = "OLD";
            $add = self::getExpression($id);
            self::insertLogExpression($action, $add);
        }
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        $db->update("expr_alias", array("name" => $expression['name']), "aliasid='$id'");
        $db->delete("expr_alias_expression", "aliasid='$id'");

        foreach ($expression['expressions'] as $expr) {
            $data = array("aliasid" => $id, "expression" => $expr);
            $db->insert("expr_alias_expression", $data);
        }
                
        if ($tabela == true) {
            //log
            $acao = "Editou expressao regular";
            self::salvaLog($acao, $id);
            $action = "NEW";
            $add = self::getExpression($id);
            self::insertLogExpression($action, $add);
        }
        
        try {
            $db->commit();
        } catch (Exception $ex) {
            $db->rollBack();
            throw $ex;
        }
    }

    public function delete($id) {
        $db = Zend_Registry::get('db');
                
        $tabela = "";
        $tabela = self::verificaLog($tabela);
        if ($tabela == true) {
            //log
            $acao = "Excluiu expressao regular";
            self::salvaLog($acao, $id);
            $action = "DEL";
            $add = self::getExpression($id);
            self::insertLogExpression($action, $add);
        }

        $db->delete("expr_alias", "aliasid='$id'");
    }

    /**
     * verificaLog - Verifica se existe módulo Loguser.
     * @param <boolean> $tabela
     * @return <boolean> True ou false
     */
    function verificaLog($tabela) {
        if (class_exists("Loguser_Manager")) {
            $tabela = true;
        } else {
            $tabela = false;
        }
        return $tabela;
    }

    /**
     * salvalog - Insere dados da ação na tabela logs.
     * @global type $id_user
     * @param <String> $ação Ação feita pelo usuário
     * @param <String> $sounds id do arquivo de som
     * @return <boolean> True ou false
     */
    function salvaLog($acao, $sounds) {
        $db = Zend_Registry::get("db");
        $ip = $_SERVER['REMOTE_ADDR'];
        $hora = date('Y-m-d H:i:s');
        $tipo = 10;
        global $id_user;

        $acao = mysql_escape_string($acao);

        $sql = "INSERT INTO `logs` VALUES (NULL, '" . $hora . "', '" . $ip . "', '" . $id_user . "', '" . $acao . "', '" . $sounds . "', '" . $tipo . "' , '" . NULL . "', '" . NULL . "', '" . NULL . "', '" . NULL . "')";

        if ($db->query($sql)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * getExpression - Monta array com todos dados da erxpressão regular
     * @param <int> $id - codigo da expressao
     * @return <array> $archive - Dados da expressao
     */
    
      function getExpression($id) {

      $archive = array();

      $db = Zend_Registry::get("db");
      $sql = "SELECT aliasid as id,name from  expr_alias where aliasid='$id'";
      $stmt = $db->query($sql);
      $archive = $stmt->fetch();
      
      $sql = "SELECT expression from  expr_alias_expression where aliasid='$id'";
      $stmt = $db->query($sql);
      $expressions = $stmt->fetchall();
      $archive["expressions"] = "";
              
      foreach($expressions as $expr){
      $archive["exp"] .= $expr["expression"] . " "; 
      }
      
      return $archive;
      } 

    /**
     * insertLogexpression - insere na tabela logs_users os dados da expressão
     * @global <int> $id_user
     * @param <array> $add
     */
    function insertLogExpression($acao, $add) {
        
        $db = Zend_Registry::get("db");
        $ip = $_SERVER['REMOTE_ADDR'];
        $hora = date('Y-m-d H:i:s');

        $auth = Zend_Auth::getInstance();
        global $id_user;

        $select = "SELECT name from peers where id = '$id_user'";
        $stmt = $db->query($select);
        $id = $stmt->fetch();

        $sql = "INSERT INTO `logs_users` VALUES (NULL, '" . $hora . "', '" . $ip . "', '" . $id["name"] . "', '" . $add["id"] . "', '" . $add["name"] . "', '" . $add["exp"] . "', '" . NULL . "', '" . "EXP" . "', '" . $acao . "')";
        $db->query($sql);
    }

}
