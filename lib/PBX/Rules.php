<?php

/**
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/lgpl.txt>.
 */

/**
 * Classe que controla a persistencia em banco de dados das regras de negócio
 * do Snep.
 *
 * Nota sobre a persistencia: O controle de persistencia é feito no snep em
 * classes separadas. Não no construtor da classe modelo como se ve em outros
 * frameworks e arquiteturas. O motivo disso é que se ocorrer uma mudança na
 * forma como é feita a persistencia desses objetos os mesmos não precisam ser
 * alterados. Isso aumenta a compactibilidade com código legado e facilita
 * migrações de código entre versões.
 *
 * @category  Snep
 * @package   Snep
 * @copyright Copyright (c) 2010 OpenS Tecnologia
 * @author Henrique Grolli Bassotto
 */
class PBX_Rules {

    private function __construct() { /* Protegendo métodos dinâmicos */
    }

    private function __destruct() { /* Protegendo métodos dinâmicos */
    }

    private function __clone() { /* Protegendo métodos dinâmicos */
    }

    /**
     * delete - Remove uma regra do banco de dados baseado no ID dela.
     * @param <int> $id
     */
    public static function delete($id) {
        $db = Zend_Registry::get('db');
        $db->delete("regras_negocio", "id='{$id}'");
    }

    /**
     * get - Obtém Regras de negócio do banco de dados.
     *
     * A REGRA É DEVOLVIDA SEM A CLASSE DE COMUNICAÇÂO COM O ASTERISK
     * @param <int> $id Numero de identificação da regra de negócio que se deseja
     * obter do banco de dados.
     * @return <object> PBX_Rule $regra de negócio corresponde ao id da chamada
     * @throws PBX_Exception_NotFound
     * @throws PBX_Exception_DatabaseIntegrity
     */
    public static function get($id) {
        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from('regras_negocio')
                ->where("id = '$id'");

        $regra_raw = $db->query($select)->fetchObject();

        if (!$regra_raw) {
            throw new PBX_Exception_NotFound("Regra $id nao encontrada");
        }

        $regra = new PBX_Rule();
        $regra->setPriority($regra_raw->prio);
        $regra->setDesc($regra_raw->desc);
        $regra->setId($id);
	$regra->setFromDialer($regra_raw->from_dialer) ;

        // Adicionando origens e destinos
        foreach (explode(',', $regra_raw->origem) as $src) {
            if (!strpos($src, ':')) {
                $regra->addSrc(array("type" => $src, "value" => ""));
            } else {
                $info = explode(':', $src);
                if (!is_array($info) OR count($info) != 2) {
                    throw new PBX_Exception_DatabaseIntegrity("Valor errado para origem da regra de negocio $regra_raw->id: {$regra_raw->origem}");
                }
                $regra->addSrc(array("type" => $info[0], "value" => $info[1]));
            }
        }
        foreach (explode(',', $regra_raw->destino) as $dst) {
            if (!strpos($dst, ':')) {
                $regra->addDst(array("type" => $dst, "value" => ""));
            } else {
                $info = explode(':', $dst);
                if (!is_array($info) OR count($info) != 2) {
                    throw new PBX_Exception_DatabaseIntegrity("Valor errado para destino da regra de negocio $regra_raw->id: {$regra_raw->destino}");
                }
                $regra->addDst(array("type" => $info[0], "value" => $info[1]));
            }
        }

        // Adicionando dias da semana
        $regra->cleanValidWeekList();
        foreach (explode(',', $regra_raw->diasDaSemana) as $diaDaSemana) {
            if ($diaDaSemana != "") {
                $regra->addWeekDay($diaDaSemana);
            }
        }

        // Adicionando validade
        foreach (explode(',', $regra_raw->validade) as $time) {
            $regra->addValidTime($time);
        }

        if (!$regra_raw->ativa) {
            $regra->disable();
        }

        if ($regra_raw->record == "1") {
            $regra->record();
        }

        $select = $db->select()
                ->from('regras_negocio_actions')
                ->where("regra_id = $id")
                ->order('prio');

        $actions = $db->query($select)->fetchAll();

        // Processando as ações das regras
        if (count($actions) > 0) {
            $select = $db->select()
                    ->from('regras_negocio_actions_config')
                    ->where("regra_id = $id")
                    ->order('prio');

            $configs_raw = $db->query($select)->fetchAll();

            // reordenando as configurações
            $configs = array();
            if (count($configs_raw) > 0) {
                foreach ($configs_raw as $config) {
                    $configs[$config['prio']][$config['key']] = $config['value'];
                }
            }

            // Adicionando cada ação da regra a Regra de negócio.
            foreach ($actions as $acao_raw) {
                $acao = $acao_raw['action'];
                if (class_exists($acao)) {
                    // Se existe configuração, pega, senão cria um array vazio.
                    $config = isset($configs[$acao_raw['prio']]) ? $configs[$acao_raw['prio']] : array();
                    $acao_object = new $acao();
                    $acao_object->setConfig($config);
                    $acao_object->setDefaultConfig(PBX_Registry::getAll($acao));
                    $regra->addAcao($acao_object);
                }
            }
        }

        return $regra;
    }

    /**
     * getAll - Retorna um array com todas as regras de negócio persistidas no snep;
     *
     * @param <string> $where
     * @return <array> $regras com todas as regras de negócio
     */
    public static function getAll($where = NULL) {
        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from('regras_negocio')
                ->order(array("prio DESC", "id"));

        if ($where != NULL) {
            $select->where($where);
        }
        //$select->where("mailing !='1' and from_dialer !='1'");
        $select->where("mailing !='1'");
        
        $stmt = $db->query($select);
        $result = $stmt->fetchAll();

        $regras = array();
        foreach ($result as $regra) {
            $regras[] = self::get($regra['id']);
        }

        return $regras;
    }

    /**
     * update - Atualiza uma regra de negócio no banco de dados.
     *
     * Para usar esse método, basta pegar o objeto da regra do banco de dados.
     * Alterar seus atributos e passá-lo a esse método.
     * 
     * @param <array> PBX_Rule $rule
     * @throws PBX_Exception_BadArg
     * @throws Exception
     */
    public static function update($rule) {
        
        $tabela = "";
        $tabela = self::verificaLog($tabela);
        if($tabela == true){
        $historico = array();
        $id_regra = $rule->getId();
        $historico = self::getRegra($id_regra);
        $exAction = self::getActions($id_regra);
        }

        if ($rule->getId() == -1) {
            throw new PBX_Exception_BadArg("Regra nao possui um id valido.");
        }

        $srcs = "";
        foreach ($rule->getSrcList() as $src) {
            $srcs .= "," . trim($src['type'] . ":" . $src['value'], ':');
        }
        $srcs = trim($srcs, ',');

        $dsts = "";
        foreach ($rule->getDstList() as $dst) {
            $dsts .= "," . trim($dst['type'] . ":" . $dst['value'], ":");
        }
        $dsts = trim($dsts, ',');

        $validade = implode(",", $rule->getValidTimeList());

        $diasDaSemana = implode(",", $rule->getValidWeekDays());

        $update_data = array(
            "prio" => $rule->getPriority(),
            "desc" => $rule->getDesc(),
            "ativa" => ($rule->isActive()) ? '1' : '0',
            "origem" => $srcs,
            "destino" => $dsts,
            "record" => $rule->isRecording(),
            "diasDaSemana" => $diasDaSemana,
            "validade" => $validade
        );

        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        try {
            $db->update("regras_negocio", $update_data, "id='{$rule->getId()}'");

            $db->delete("regras_negocio_actions", "regra_id='{$rule->getId()}'");
            $action_prio = 0;
            foreach ($rule->getAcoes() as $acao) {
                $action_update_data = array(
                    "regra_id" => $rule->getId(),
                    "prio" => $action_prio,
                    "action" => get_class($acao)
                );
                $db->insert("regras_negocio_actions", $action_update_data);

                foreach ($acao->getConfigArray() as $chave => $valor) {
                    if (!is_null($valor)) {
                        $action_config_data = array(
                            "regra_id" => $rule->getId(),
                            "prio" => $action_prio,
                            "key" => $chave,
                            "value" => $valor
                        );
                        $db->insert("regras_negocio_actions_config", $action_config_data);
                    }
                }

                $action_prio++;
            }
            $db->commit();

        } catch (Exception $ex) {
            $db->rollBack();
            throw $ex;
        }
        
        
        $tabela = self::verificaLog($tabela);
        if($tabela == true){
        $regra_update = self::getRegra($id_regra);
        
        self::insertLogRegra($historico, $regra_update);
        }
 
    }
    
    /**
     * getActions - Monta array com ações da regra
     * @param <int> $id
     * @return <array> Array de ações
     */
    function getActions($id) {

        $db = Zend_Registry::get("db");
        $sql = "SELECT action from regras_negocio_actions where regras_negocio_actions.regra_id = '$id' ";
        $stmt = $db->query($sql);
        $acao = $stmt->fetchAll();

        foreach ($acao as $item => $value) {
            $acao .= "," . $value["action"];
        }
        $exacao = explode(",", $acao);
        
        return $exacao;
    }

    /**
     * getRegra - Monta array com todos dados da regra de negócios
     * @param <int> $id - Código da regra
     * @return <array> $regra - Dados da regra
     */
    function getRegra($id) {

        $regra = array();
        $action = array();

        $db = Zend_Registry::get("db");
        $sql = "SELECT * from  regras_negocio where id='$id'";
        $stmt = $db->query($sql);
        $regra = $stmt->fetch();

        $sql = "SELECT * FROM `regras_negocio_actions` where `regra_id`='$id'";
        $stmt = $db->query($sql);
        $acoes = $stmt->fetchall();

        $sql = "SELECT * FROM `regras_negocio_actions_config` where `regra_id`='$id'";
        $stmt = $db->query($sql);
        $valores = $stmt->fetchall();

        foreach ($acoes as $item => $acao) {
            foreach ($valores as $key => $valor) {

                $regra["acoes"][$item]["prio"] = $acao["prio"];
                $regra["acoes"][$item]["action"] = $acao["action"];
                if ($acao["prio"] == $valor["prio"]) {

                    $regra["acoes"][$item]["key"] .= $valor["key"] . " | ";
                    $regra["acoes"][$item]["value"] .= $valor["value"] . " | ";
                }
            }
        }
        return $regra;
    }
    
    /**
     * insertLogRegra - insere na tabela logs_regra quando é modificada regra
     * @global <int> $id_user
     * @param <array> $historico
     * @param <array> $regra_update
     */
    function insertLogRegra($historico, $regra_update) {

        $db = Zend_Registry::get("db");
        $ip = $_SERVER['REMOTE_ADDR'];
        $hora = date('Y-m-d H:i:s');

        $auth = Zend_Auth::getInstance();
        global $id_user;
        $tipo = 1;
        $acao = "Editou regra";
        $actions_hist = $historico['acoes'];
        $actions_up = $regra_update['acoes'];


        $select = "SELECT name from peers where id = '$id_user'";
        $stmt = $db->query($select);
        $id = $stmt->fetch();

        //add historico
        foreach ($actions_hist as $number => $item) {
            
            // Pega somente nome da ação. Ex: DiscarRamal de PBX_Rule_Action_DiscarRamal
            if(strpos($item['action'],"_") !== false){
            $action = $item['action'];
            $action = explode("_", $action);
            $action = $action[3];
            }else{
            // Ação ARS não possui PBX_Rule_Action_ no nome da ação
            $action = $item['action'];    
            }        
            
            $sql = "INSERT INTO `logs_regra` VALUES (NULL, '" . $historico["id"] . "', '" . $hora . "', '" . $ip . "', '" . $id["name"] . "', '" . $acao . "', '" . $historico["prio"] . "' , '" . $historico["desc"] . "', '" . $historico["origem"] . "', '" . $historico["destino"] . "', '" . $historico["validade"] . "', '" . $historico["diasDaSemana"] . "', '" . $historico["record"] . "', '" . $historico["ativa"] . "', '" . $action . "', '" . $item["prio"] . "', '" . $item["key"] . "', '" . $item["value"] . "', '" . "OLD" . "')";
            $db->query($sql);
        }

        //add update
        foreach ($actions_up as $number => $up) {
            
            // Pega somente nome da ação. Ex: DiscarRamal de PBX_Rule_Action_DiscarRamal
            if(strpos($up['action'],"_") !== false){
            $action = $up['action'];
            $action = explode("_", $action);
            $action = $action[3];
            }else{
            // Ação ARS não possui PBX_Rule_Action_ no nome da ação
            $action = $up['action'];    
            }
            
            $sql = "INSERT INTO `logs_regra` VALUES (NULL, '" . $regra_update["id"] . "', '" . $hora . "', '" . $ip . "', '" . $id["name"] . "', '" . $acao . "', '" . $regra_update["prio"] . "' , '" . $regra_update["desc"] . "', '" . $regra_update["origem"] . "', '" . $regra_update["destino"] . "', '" . $regra_update["validade"] . "', '" . $regra_update["diasDaSemana"] . "', '" . $regra_update["record"] . "', '" . $regra_update["ativa"] . "', '" . $action . "', '" . $up["prio"] . "', '" . $up["key"] . "', '" . $up["value"] . "', '" . "NEW" . "')";
            $db->query($sql);
        }
    }

    /**
     * verificaLog - Verifica se existe módulo Loguser
     * @param <boolean> $tabela
     * @return <boolean> True ou false
     */
    function verificaLog($tabela) {
        if (class_exists("Loguser_Manager")) {
            $tabela = true;
        } else {
            $tabela = false;
        }
        return $tabela;
    }

    /**
     * register - Cadastra uma regra de negócio no banco de dados do Snep.
     * @param <array> PBX_Rule $rule
     * @throws Exception
     */
    public static function register($rule) {

        $srcs = "";
        foreach ($rule->getSrcList() as $src) {
            $srcs .= "," . trim($src['type'] . ":" . $src['value'], ':');
        }
        $srcs = trim($srcs, ',');

        $dsts = "";
        foreach ($rule->getDstList() as $dst) {
            $dsts .= "," . trim($dst['type'] . ":" . $dst['value'], ':');
        }
        $dsts = trim($dsts, ',');

        $validade = implode(",", $rule->getValidTimeList());
        $mailing = $rule->getMailing();
        $fromDialer = $rule->getFromDialer();

        $diasDaSemana = implode(",", $rule->getValidWeekDays());

        $insert_data = array(
            "prio" => $rule->getPriority(),
            "desc" => $rule->getDesc(),
            "origem" => $srcs,
            "destino" => $dsts,
            "validade" => $validade,
            "diasDaSemana" => $diasDaSemana,
            "mailing" => $mailing,
            "from_dialer" => $fromDialer,
            "record" => $rule->isRecording()
        );

        $db = Zend_Registry::get('db');

        $db->beginTransaction();

        try {
            $db->insert("regras_negocio", $insert_data);

            $rule->setId((int) $db->lastInsertId('regras_negocio_id'));

            $action_prio = 0;
            foreach ($rule->getAcoes() as $acao) {
                $action_insert_data = array(
                    "regra_id" => $rule->getId(),
                    "prio" => $action_prio,
                    "action" => get_class($acao)
                );
                $db->insert("regras_negocio_actions", $action_insert_data);

                foreach ($acao->getConfigArray() as $chave => $valor) {
                    $action_config_data = array(
                        "regra_id" => $rule->getId(),
                        "prio" => $action_prio,
                        "key" => $chave,
                        "value" => $valor
                    );
                    $db->insert("regras_negocio_actions_config", $action_config_data);
                }
                $action_prio++;
            }
            $db->commit();
        } catch (Exception $ex) {
            $db->rollBack();
            throw $ex;
        }
    }

}
