<?php
/**
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/lgpl.txt>.
 */

/**
 * Classe que abstrai as tarifas
 *
 * @see Snep_Tarifas
 *
 * @category  Snep
 * @package   Snep
 * @copyright Copyright (c) 2010 OpenS Tecnologia
 * @author    Rafael Pereira Bozzetti <rafael@opens.com.br>
 * 
 */
class Snep_Cnl {
    
     /**
     * delPrefixo - Deleta os dados da tabela ars_prefixo 
     * @return type
     * @throws Exception
     */
    public static function delPrefixo() {

        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        try {

            $db->delete('ars_prefixo');
            $db->commit();

        } catch (Exception $ex) {

            $db->rollBack();
                throw $ex;
        }
        return;
    }
    
    /**
     * delCidade - Deleta os dados da tabela ars_cidade
     * @return type
     * @throws Exception
     */
    public static function delCidade() {

        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        try {

            $db->delete('ars_cidade');
            $db->commit();

        } catch (Exception $ex) {

            $db->rollBack();
                throw $ex;
        }
        return;
    }
    
     /**
     * delDDD - Deleta os dados da tabela ars_ddd 
     * @return type
     * @throws Exception
     */
    public static function delDDD() {

        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        try {

            $db->delete('ars_ddd');
            $db->commit();

        } catch (Exception $ex) {

            $db->rollBack();
                throw $ex;
        }
        return;
    }
    
     /**
     * delOperadora - Deleta os dados da tabela ars_operadora  
     * @return type
     * @throws Exception
     */
    public static function delOperadora() {

        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        try {

            $db->delete('ars_operadora');
            $db->commit();

        } catch (Exception $ex) {

            $db->rollBack();
                throw $ex;
        }
        return;
    }
    
     /**
     * addOperadora - Adiciona dados na tabela ars_operadora
     * @param <int> $id - Código da operadora 
     * @param <String> $data - Nome da operadora 
     * @throws Exception
     */
    public static function addOperadora($id,$data) {

        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        try {

            $operadora = array('id' => $id , 'name' => $data);
            $db->insert('ars_operadora', $operadora);
            $db->commit();

        } catch (Exception $ex) {

            $db->rollBack();
            throw $ex;
        }
    }
    
     /**
     * addDDD - Adiciona dados na tabela ars_ddd
     * @param <int> $cod - Valor DDD
     * @param <String> $estado - Sigla do estado
     * @param <int>  $cidade - Código da cidade
     * @throws Exception
     */
    public static function addDDD($cod,$estado,$cidade) {

        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        try {

            $ddd = array('cod' => $cod,'estado' => $estado,'cidade' => $cidade);
            $db->insert('ars_ddd', $ddd);

            $db->commit();

        } catch (Exception $ex) {
            $db->rollBack();
            throw $ex;
        }
    }
    
     /**
     * addCidade - Adiciona dados na tabela ars_cidade 
     * @param <String> $name - Nome da cidade
     * @return type
     * @throws Exception
     */
    public static function addCidade($name) {

        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        try {

            $cidade = array('name' => $name);
            $db->insert('ars_cidade', $cidade);
            $id = $db->lastInsertId();

            $db->commit();

        } catch (Exception $ex) {
            $db->rollBack();
            throw $ex;
        }
        return $id;
    }
    
    /**
     * addPrefixo - Adiciona dados na tabela ars_prefixo
     * @param <int> $prefixo - Valor do prefixo
     * @param <int> $cidade - Código da cidade
     * @param <int> $operadora - Código da operadora 
     * @throws Exception
     */
    public static function addPrefixo($prefixo,$cidade,$operadora) {//600, '23', 4

        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        try {

            $addPrefixo = array('prefixo' => $prefixo,'cidade' => $cidade,'operadora' => $operadora);
            $db->insert('ars_prefixo', $addPrefixo);

            $db->commit();

        } catch (Exception $ex) {
            $db->rollBack();
            throw $ex;
        }
    }
    
    /**
     * getCnl - 
     * @return type
     */
    public static function getCnl() {

        $db = Zend_Registry::get('db');
        $select = $db->select()
                        ->from('cnl');

        $stmt = $db->query($select);
        $registros = $stmt->fetchAll();

        return $registros;
    }
    
     /**
     * getOperadora - Busca dados da tabela ars_operadora 
     * @return type
     */
    public static function getOperadora() {

        $db = Zend_Registry::get('db');
        $select = $db->select()
                        ->from('ars_operadora');

        $stmt = $db->query($select);
        $registros = $stmt->fetchAll();

        return $registros;
    }
    
    /**
     * get - Busca dados das tabelas ars_dd e ars_cidade
     * @param <String> $uf - Sigla do estado
     * @return type
     */
    public function get($uf) {

        $db = Zend_Registry::get('db');

        $select = $db->select()
        ->from(array('ddd' => 'ars_ddd'),array('estado as uf'))
        ->join(array('cid' => 'ars_cidade'), 'cid.id = ddd.cidade' ,array('name as municipio'))
        ->where("ddd.estado = '$uf'")
        ->order("municipio");

        $stmt = $db->query($select);
        $registros = $stmt->fetchAll();

        return $registros;
    }
    
     /**
     * getPrefixo - Busca dados das tabelas ars_cidade e ars_prefixo
     * @param <String> $cidade - Nome da cidade 
     * @return type
     */
    public function getPrefixo($cidade) {
        $db = Zend_Registry::get('db');

        $select = $db->select()
        ->from(array('cid' => 'ars_cidade'),array('name as municipio'))
        ->join(array('pre' => 'ars_prefixo'), 'pre.cidade = cid.id' ,array('prefixo'))
        ->where("cid.name = '$cidade'");

        $stmt = $db->query($select);
        $registros = $stmt->fetchAll();

        return $registros;
    }
    
     /**
     * getCarrierByPrefix - Busca dados das tabelas ars_prefixo, ars_operadora e ars_ddd  
     * @param <int> $prefix - Valor do DDD
     * @return type
     */
    public static function getCarrierByPrefix($prefix) {
        $db = Zend_Registry::get('db');
        $ddd = substr($prefix, 0, 2);
        $prefixo = substr($prefix, 2, 4);
        $sql = sprintf("select op.id, op.name from ars_prefixo p 
                        inner join ars_operadora op on p.operadora = op.id 
                        inner join ars_ddd ddd on p.cidade = ddd.cidade
                        where p.prefixo = '%s'
                        and ddd.cod = '%s'", $prefixo, $ddd);
        $stmt = $db->query($sql);
        $data = $stmt->fetch();
        $stmt->fetchAll();
        return $data;
    }
}
