<?php
/**
 *  This file is part of SNEP.
 *  Para território Brasileiro leia LICENCA_BR.txt
 *  All other countries read the following disclaimer
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/lgpl.txt>.
 */

/**
 * Classe que abstrai os Vínculos
 *
 * @see Snep_Vinculos
 *
 * @category  Snep
 * @package   Snep
 * @copyright Copyright (c) 2010 OpenS Tecnologia
 * @author    Rafael Pereira Bozzetti <rafael@opens.com.br>
 *
 */
class Snep_Vinculos {    

    public function __construct() {}
    public function __destruct() {}
    public function __clone() {}

    public function __get($atributo) {
        return $this->{$atributo};
    }
    
    /**
     * getVinculos - Busca ramais vinculados
     * @param <String> $ramal
     * @return <array>
     */
    public function getVinculos($ramal) {

        $db = Zend_Registry::get('db');

        $select = $db->select()
        ->from('permissoes_vinculos', array('id_peer', 'tipo', 'id_vinculado'))
        ->where("permissoes_vinculos.id_peer='$ramal'");

        $stmt = $db->query($select);
        $vinculos = $stmt->fetchAll();

        $doramal = array();
        
        foreach($vinculos as $reg) {
            $doramal[$reg['id_vinculado']] = $reg['id_vinculado'];            
        }
        
        return $doramal;
    }

    /**
     * getRamaisDoGrupo - Busca ramais dos grupos vinculados
     * @param <String> $grupo
     * @return type
     */
    public function getRamaisDoGrupo($grupo) {

        $db = Zend_Registry::get('db');

        $select = $db->select()
        ->from('peers', array('name') )
        ->where("peers.peer_type = 'R' ")
        ->where("peers.group = '$grupo'");

        $stmt = $db->query($select);
        $vinculos = $stmt->fetchAll();

        $return = array();
        foreach($vinculos as $ramais) {
            $return[$ramais['name']] = $ramais['name'];
        }        
        return $return;
    }

    /**
    * getNivelVinculos - Busca ramais vinculados ao usuário para expor no relatório 
    * @param <int> $ramal Ramal ou agente que possui os vinculos
    * @return <string> Ramais, Agente e grupos vinculados ao usuário
    */
    public function getNivelVinculos($ramal) {

        if ($ramal == "admin") {
            return 1;
        }
        $db = Zend_Registry::get('db');

        $select = $db->select()
        ->from('permissoes_vinculos' )
        ->where("permissoes_vinculos.id_peer = '$ramal'")
        ->order('permissoes_vinculos.tipo');

        $stmt = $db->query($select);
        $vinculos = $stmt->fetchAll();

        $ramais  = '';
        $grupos  = '';
        $agentes = '';
        
        foreach($vinculos as $vinculo) {
            if($vinculo['tipo'] == "R") {
                $ramais .= "{$vinculo['id_vinculado']}, ";
            }
            if($vinculo['tipo'] == "A") {
                $agentes .= "{$vinculo['id_vinculado']}, ";
            }
            elseif($vinculo['tipo'] == "G") {
                $grupos .= "{$vinculo['id_vinculado']}, ";
            }
        }

        ( $agentes == '' ? 0 : $agentes = "  Agentes: ". substr($agentes, 0, -2) );
        ( $ramais  == '' ? 0 : $ramais  = " Ramais: ". substr($ramais, 0, -2) );
        ( $grupos  == '' ? 0 : $grupos  = " Grupos: ". substr($grupos, 0, -2) );
        
        return $ramais . $agentes . $grupos;
    }
    
    /**
    * setVinculos - Insere dados dos vinculos na tabela permissoes_vinculos
    * @param <int> $ramal Ramal ou agente que possui os vinculos
    * @param <String> $tipo Tipo podendo ser R para ramal e A para agente e G para grupo
    * @param <int> $vinculo ramal ou agente a ser vinculado 
    * @return <boolean> True ou false
    */
    public function setVinculos($ramal, $tipo, $vinculo) {

        $db = Zend_Registry::get('db');

        $insert_data = array("id_peer" => $ramal,
                             "tipo"    => $tipo,
                             "id_vinculado"   =>  trim( $vinculo )
        );
        $db->insert('permissoes_vinculos', $insert_data);
        
    }
    
    /**
    * getVinculados - Retorna ramais vinculados 
    * @param <String> $ramal - Ramal que possui vinculo
    * @return <Array> ramais vinculados
    */
    public function getVinculados($ramal) {

        $db = Zend_Registry::get('db');

        $select = $db->select()
        ->from('permissoes_vinculos', array('id_peer', 'id_vinculado'))
        ->from('peers', array('callerid'))
        ->where("permissoes_vinculos.id_peer='$ramal'")
        ->where("permissoes_vinculos.tipo = 'R' ")
        ->where("peers.name=permissoes_vinculos.id_peer");
        
        $stmt = $db->query($select);
        $vinculos = $stmt->fetchAll();

        return $vinculos;
    }
    
    /**
    * getDesvinculados - Retorna ramais não vinculados 
    * @param <String> $ramal - Ramal a ser consultado
    * @return <Array> Ramais desvinculados
    */
    public function getDesvinculados($ramal) {

        $db = Zend_Registry::get('db');

        $select = $db->select()
        ->from('permissoes_vinculos', array('id_vinculado'))
        ->where("permissoes_vinculos.id_peer='$ramal' ")
        ->where("permissoes_vinculos.tipo='R'");

        $stmt = $db->query($select);
        $vinculos = $stmt->fetchAll();        

        $des = $db->select()
        ->from("peers", array('name'))
        ->where("name != 'admin' ")
        ->where("peer_type = 'R' ");

        if($vinculos) {
            $des->where('name NOT IN (?)', $vinculos);
        }        

        $stmt = $db->query($des);
        $desvinc = $stmt->fetchAll();
        
        return $desvinc; 
    }
    
    /**
    * resetVinculos - Deleta vinculo do ramal 
    * @param <String> $name - Ramal que possui vinculo
    */
    public function resetVinculos($name) {

        $db = Zend_Registry::get('db');

        $db->beginTransaction() ;
        try {
            $db->delete('permissoes_vinculos', "id_peer = '$name'");
            $db->commit();
        }
        catch(Exception $e) {
            $db->rollBack();
        }
    }
    
    /**
    * setVinculosGrupo - Insere grupo vinculado na tabela permissoes_vinculos 
    * @param <String> $ramal - Ramal que possui vinculo
    * @param <Array> $arrGrupos - Array de grupos
    */
    public function setVinculosGrupo($ramal, $arrGrupos) {

        $db = Zend_Registry::get('db');

        foreach($arrGrupos as $num => $grupo) {
            $insert_data = array("id_peer" => $ramal,
                                 "tipo"   => "G",
                                 "id_vinculado"   => trim( $grupo )
            );
            $db->insert('permissoes_vinculos', $insert_data);
        }
    }
    
    /**
    * getVinculadosGrupo - Retorna grupos vinculados 
    * @param <String> $ramal - Ramal que possui vinculo
    * @return <Array> grupos vinculados
    */
    public function getVinculadosGrupo($ramal) {
        
        $db = Zend_Registry::get('db');
        
        $select = $db->select()
        ->from("permissoes_vinculos")
        ->where("tipo = 'G' ")
        ->where("id_peer = '$ramal'");
        
        $stmt = $db->query($select);
        $grupo_vinculado = $stmt->fetchAll();

        return $grupo_vinculado;
    }
    
    /**
    * getDesvinculadosGrupo - Retorna grupos desvinculados 
    * @param <String> $ramal - Ramal a ser consultado
    * @return <Array> grupos desvinculados
    */
    public function getDesvinculadosGrupo($ramal) {

        $db = Zend_Registry::get('db');

        $select = $db->select()
        ->from("permissoes_vinculos")
        ->where("tipo = 'G' ")
        ->where("id_peer = '$ramal'");

        $stmt = $db->query($select);
        $vinculo = $stmt->fetchAll();

        $des = $db->select()
        ->from("groups", array('name'));

        if($vinculo) {
            $des->where('name NOT IN (?)', $vinculo);
        }
        
        $stmt = $db->query($des);
        $desvinc = $stmt->fetchAll();

        return $desvinc;
    }

    /**
    * getVinculadosAgente - Retorna agentes vinculados 
    * @param <String> $ramal - Agente que possui vinculo
    * @return <Array> Agentes vinculados
    */
    public function getVinculadosAgente($ramal) {

        $agentes = self::getAgente();

        $db = Zend_Registry::get('db');

        $select = $db->select()
        ->from("permissoes_vinculos")
        ->where("tipo = 'A' ")
        ->where("id_peer = '$ramal'");        

        $stmt = $db->query($select);
        $agentes_vinculado = $stmt->fetchAll();

        return $agentes_vinculado;
    }
    
    /**
    * getDesvinculadosAgente - Retorna agentes desvinculados 
    * @param <String> $ramal - agente a ser consultado
    * @return <Array> agentes vinculados
    */
    public function getDesvinculadosAgente($ramal) {

        $db = Zend_Registry::get('db');

        $select = $db->select()
        ->from("permissoes_vinculos")
        ->where("tipo = 'A' ")
        ->where("id_peer = '$ramal'");

        $stmt = $db->query($select);
        $vinculado = $stmt->fetchAll();

        $agentes = self::getAgente();
        $desvinculados = array();

        foreach($agentes as $agent) {
            if(!array_key_exists($agent, $vinculado)) {
                $desvinculados[$agent] = $agent;
            }            
        }


        return $desvinculados;        
    }
    
    /**
    * getAgente - Retorna agentes cadastrados 
    * @return <Array> Agentes cadastrados
    */
    public function getAgente() {

        $arquivo =  file_get_contents('/etc/asterisk/snep/snep-agents.conf');
        $teste = explode("\n", $arquivo);
        $f = false;

        $agents = array();
        foreach($teste as $id => $agent) {
            if($id > 12 ) {
                if (! strlen( trim($agent) ) < 1 ) {
                    $swp = substr($agent, strpos($agent, "=>")+3);
                    $numero = substr($swp, 0, strpos($swp, ","));
                    $agents[$numero] = $numero;
                }                    
            }            
        }

        return  $agents;
    }
}

?>
